#
# sage
#


### globle config ###

gf = GF(16)
v1 = 32
o1 = 32
o2 = 32

v2 = v1 + o1
n = v1+o1+o2
m = o1+o2 

def n_terms_trimat( dim ):
  return dim*(dim+1)//2

D1 = n_terms_trimat(v1) + v1*o1
D2 = n_terms_trimat(v2) + v2*o2

keyformat_column_major = true


##############

len_salt = 16
len_gfsalt = len_salt if GF(256)==gf else len_salt*2

len_hash = 32


##################################################
###
###   GFs <--> int convertors
###
##################################################


def uint4_to_gf16( a ):
  my_gf = GF(16)
  if 0 == a : return my_gf(0)
  log_tab = [ -42, 0, 5, 10, 1, 4, 2, 8, 6, 13, 9, 7, 11, 12, 3, 14 ]
  return my_gf.0^log_tab[a]

def gf16_to_uint4( a ):
  log_val = int( a._log_repr() );
  if 0 == log_val : return 0
  exp_tab = [ 0x1,0x4,0x6,0xe,0x5,0x2,0x8,0xb,0x7,0xa,0x3,0xc,0xd,0x9,0xf,0x1 ]
  return exp_tab[log_val];

#################

def u8list_to_u4list( al ):
  r = []
  for i in range(len(al)):
    il = al[i]%16
    ih = al[i]>>4
    r.append(il)
    r.append(ih)
  return r

def u4list_to_u8list( al ):
  r = []
  for i in range(0,len(al),2):
    il = al[i]
    ih = al[i+1]
    r.append( il + (ih<<4) )
  return r


##################
### XXX: test required.

def uint8_to_gf256( a ):
  my_gf = GF(256)
  if 0 == a : return my_gf(0)
  log_tab = [ 255,   0,  85, 170,  17,  68,  34, 136, 102, 221, 153, 119, 187, 204,  51, 238, 171, 186,  73, 148, 234, 174,  82,  37,  87, 117,  41, 146,  74, 164, 213,  93,   1, 233,  16, 158, 159, 178, 249,  43,  64, 122,   4, 167, 172, 231, 202, 126,  86, 243,  63, 101,   2, 211,  61,  32, 244, 128,   8,  79, 149, 252, 207,  89, 188, 110, 191,  58, 203, 230, 251, 163,  90, 181,  54, 104, 165,  91,  99, 134, 239, 142,  47, 155, 232, 254, 185, 242, 150, 109, 141,  26, 214, 105, 161, 216, 205, 151, 247,  71, 127, 116, 220, 121, 208, 108, 180, 107,  75, 182,  13, 198,  52,  27,  45, 218, 173, 210, 177,  67, 253, 209, 115, 229,  94,  55,  29, 223,  18, 143, 195,  21, 250, 219, 176, 184,  33, 248,  60,  81, 175, 189,  11, 139, 137,  48, 112, 130, 179,  53, 140, 114,   3, 152,  40,   7,  83,  59,  39, 200,  69, 240, 227, 132,  44,  46, 190, 246,  62,  72,  84,  15, 235, 111, 194, 226,  35, 156, 236,  77, 160,  28,  12,  98, 212, 206, 201,  50,  38, 192, 193,  10, 103, 106, 228,  25,   5, 224,  19,  96,  80,  14,  49,   6, 118, 166,  78, 145, 120, 162, 241,  66, 123,  95,  22,  23, 245, 183, 113,  97,  42, 135,  36,  31, 222, 215, 133, 197, 168,  30, 144, 124,   9, 199, 138, 225,  88,  92, 237, 125, 154, 217,  70,  57,  65,  56, 196,  24, 129,  76, 131,  20, 147, 100, 157, 169 ]
  return my_gf.0^log_tab[a]

def gf256_to_uint8( a ):
  log_val = int( a._log_repr() );
  if 0 == log_val : return 0
  exp_tab = [ 0x01, 0x20, 0x34, 0x98, 0x2a, 0xc4, 0xcb, 0x9b, 0x3a, 0xe8, 0xbf, 0x8e, 0xb6, 0x6e, 0xc9, 0xab, 0x22, 0x04, 0x80, 0xc6, 0xfb, 0x83, 0xd6, 0xd7, 0xf7, 0xc3, 0x5b, 0x71, 0xb5, 0x7e, 0xe5, 0xdf, 0x37, 0x88, 0x06, 0xb0, 0xde, 0x17, 0xbc, 0x9e, 0x9a, 0x1a, 0xdc, 0x27, 0xa4, 0x72, 0xa5, 0x52, 0x91, 0xca, 0xbb, 0x0e, 0x70, 0x95, 0x4a, 0x7d, 0xf5, 0xf3, 0x43, 0x9d, 0x8a, 0x36, 0xa8, 0x32, 0x28, 0xf4, 0xd3, 0x77, 0x05, 0xa0, 0xf2, 0x63, 0xa9, 0x12, 0x1c, 0x6c, 0xf9, 0xb3, 0xce, 0x3b, 0xc8, 0x8b, 0x16, 0x9c, 0xaa, 0x02, 0x30, 0x18, 0xec, 0x3f, 0x48, 0x4d, 0xed, 0x1f, 0x7c, 0xd5, 0xc7, 0xdb, 0xb7, 0x4e, 0xfd, 0x33, 0x08, 0xc0, 0x4b, 0x5d, 0xc1, 0x6b, 0x69, 0x59, 0x41, 0xad, 0x92, 0xda, 0x97, 0x7a, 0x65, 0x19, 0xcc, 0x0b, 0xd0, 0x67, 0x29, 0xd4, 0xe7, 0xef, 0x2f, 0x64, 0x39, 0xf8, 0x93, 0xfa, 0xa3, 0xe2, 0x4f, 0xdd, 0x07, 0x90, 0xea, 0x8f, 0x96, 0x5a, 0x51, 0x81, 0xe6, 0xcf, 0x1b, 0xfc, 0x13, 0x3c, 0x58, 0x61, 0x99, 0x0a, 0xf0, 0x53, 0xb1, 0xfe, 0x23, 0x24, 0xb4, 0x5e, 0xd1, 0x47, 0x1d, 0x4c, 0xcd, 0x2b, 0xe4, 0xff, 0x03, 0x10, 0x2c, 0x74, 0x15, 0x8c, 0x86, 0x76, 0x25, 0x94, 0x6a, 0x49, 0x6d, 0xd9, 0x87, 0x56, 0x11, 0x0c, 0x40, 0x8d, 0xa6, 0x42, 0xbd, 0xbe, 0xae, 0x82, 0xf6, 0xe3, 0x6f, 0xe9, 0x9f, 0xba, 0x2e, 0x44, 0x0d, 0x60, 0xb9, 0x3e, 0x68, 0x79, 0x75, 0x35, 0xb8, 0x1e, 0x5c, 0xe1, 0x5f, 0xf1, 0x73, 0x85, 0x66, 0x09, 0xe0, 0x7f, 0xc5, 0xeb, 0xaf, 0xa2, 0xc2, 0x7b, 0x45, 0x2d, 0x54, 0x21, 0x14, 0xac, 0xb2, 0xee, 0x0f, 0x50, 0xa1, 0xd2, 0x57, 0x31, 0x38, 0xd8, 0xa7, 0x62, 0x89, 0x26, 0x84, 0x46, 0x3d, 0x78, 0x55, 0x01 ]
  return exp_tab[log_val];


###################

import struct

def gfs_to_bytes( inp ):
  list_u8 = inp if GF(256) == gf else u4list_to_u8list( map( gf16_to_uint4 , inp ) )
  return struct.pack( 'B'*len(list_u8) , *list_u8 )


def bytes_to_gfs( inp ):
  list_u8 = struct.unpack( 'B'*len(inp) , inp )
  return map( uint8_to_gf256 , list_u8 ) if GF(256) == gf else map(  uint4_to_gf16 , u8list_to_u4list( list_u8 ) )

###################

import binascii

def hex_to_gfs( hexs ):
  return bytes_to_gfs(binascii.unhexlify(hexs))

def gf_hexlify( gfs ) :
  return binascii.hexlify( gfs_to_bytes( gfs ))


###########################
###
### hash
###
###########################


def sha256(msg):
  import hashlib;
  return hashlib.sha256(msg).digest()

def gf_hash( inp ):
  hr = sha256( gfs_to_bytes(inp) )
  return bytes_to_gfs( hr )



########################
###
###  prng
###
########################


def prng_set_gfhash( seed ):
  return [ gf_hash( seed ) , 0 ]

def prng_gen_gfhash( prng_state , length ):
  ret = []
  st = copy( prng_state )
  while length > 0 :
    if len(st[0]) == st[1] : st = prng_set( st[0] )
    ret.append( st[0][st[1]] )
    st[1] = st[1] + 1
    length = length - 1
  return ret, st


"""
New prng

p = prng_set2(b'test seed'); x, p = prng_gen2(p, 1234); y, p = prng_gen2(p, 4321)
p2 = prng_set2(b'test seed'); z, p2 = prng_gen2(p2, 5555)

assert x + y == z


import drbg

def prng_set2(seed_bytes):
    return [drbg.CTRDRBG('aes256', seed_bytes), '']

def prng_gen2(prng_state, length):
    ret = []
    st = copy(prng_state)
    while length > len(st[1]):
        st[1] += st[0].generate(1024)
    ret = st[1][:length]
    st[1] = st[1][length:]
    return ret, st


def adapter_prng_set2( gf_seed ):
  return prng_set2( gfs_to_bytes(gf_seed) )

def adapter_prng_gen2( prng_state , length ):
  assert 0 == (length % 2 )
  len_byte = length // 2
  r_list , r_state = prng_gen2( prng_state , len_byte )
  return bytes_to_gfs(r_list), r_state

"""





########################
###
###  prng interface
###
########################

def prng_set( gf_seed ):
  return prng_set_gfhash( gf_seed )
  #return adapter_prng_set2( gf_seed )


def prng_gen( prng_state , length ):
  return prng_gen_gfhash( prng_state , length )
  #return adapter_prng_gen2( prng_state , length )


##################################


###########################
### homogeneous quadratic polynomials
###########################

def trimat( dim , l ):
  st = 0
  r = []
  for i in range(dim):
    r = r + [gf(0)]*i + l[st:st+dim-i]
    st = st + dim - i
  return matrix( gf , dim , r )

def trimat_tolist( mat ):
  r = []
  for i in range(mat.nrows()):
    r = r + list(mat[i])[i:mat.nrows()]
  return r


def eval_trimat( trimat , x ):
  return vector(x) * trimat * vector(x)

def eval_recmat( mat , x_row , x_col ):
  return vector(x_row)* mat * vector(x_col)


####################################


def trimats_to_macaulaymat( tris ):
  return matrix( map( trimat_tolist , tris ) )

def trimats_to_macaulaymat_cols( tris ):
  return trimats_to_macaulaymat( tris ).transpose().rows()

def macaulaymat_to_trimats( mat , dim_trimat ):
  return [ trimat(dim_trimat, list(mat[i])) for i in range(mat.nrows()) ]


def macaulaymat_to_recmats( mat , height_rec , width_rec ):
  return [ matrix(gf,height_rec,width_rec,list(mat[i])) for i in range(mat.nrows()) ]


def mat_to_list( mat ):
  r = []
  for row in mat.rows():
    r = r + list(row)
  return r

def recmats_to_macaulaymat( recs ):
  return matrix( map(mat_to_list, recs ))

def recmats_to_macaulaymat_cols( recs ):
  return recmats_to_macaulaymat( recs ).transpose().rows()



############################################



### key related functions


class Pubkey:
  F1 = MatrixSpace( gf , v1 , v1 )
  F2 = MatrixSpace( gf , v1 , o1 )
  F3 = MatrixSpace( gf , v1 , o2 )
  F5 = MatrixSpace( gf , o1 , o1 )
  F6 = MatrixSpace( gf , o1 , o2 )
  F9 = MatrixSpace( gf , o2 , o2 )

  def __init__(self):
    # B1, generated by pk_seed in the cyclic version
    self.l1_F1s = [ Seckey.F1(0) for i in range(o1) ]
    self.l1_F2s = [ Seckey.F2(0) for i in range(o1) ]
    self.l2_F1s = [ Seckey.F1(0) for i in range(o2) ]
    self.l2_F2s = [ Seckey.F2(0) for i in range(o2) ]

    # B2, generated by pk_seed in the cyclic version
    self.l2_F3s = [ Seckey.F3(0) for i in range(o2) ]
    self.l2_F5s = [ Seckey.F5(0) for i in range(o2) ]
    self.l2_F6s = [ Seckey.F6(0) for i in range(o2) ]

    # un-compressable parts
    self.l1_F3s = [ Seckey.F3(0) for i in range(o1) ]
    self.l1_F5s = [ Seckey.F5(0) for i in range(o1) ]
    self.l1_F6s = [ Seckey.F6(0) for i in range(o1) ]
    self.l1_F9s = [ Seckey.F9(0) for i in range(o1) ]

    self.l2_F9s = [ Seckey.F9(0) for i in range(o2) ]



class Seckey:
  Mt = MatrixSpace( gf , n , n )
  Mt1 = MatrixSpace( gf , v1 , o1 )
  Mt2 = MatrixSpace( gf , v1 , o2 )
  Mt3 = MatrixSpace( gf , o1 , o2 )

  Ms = MatrixSpace( gf , m , m )
  Ms1 = MatrixSpace( gf , o1 , o2 )

  F1 = MatrixSpace( gf , v1 , v1 )
  F2 = MatrixSpace( gf , v1 , o1 )
  F3 = MatrixSpace( gf , v1 , o2 )
  F5 = MatrixSpace( gf , o1 , o1 )
  F6 = MatrixSpace( gf , o1 , o2 )
  F9 = MatrixSpace( gf , o2 , o2 )

  def __init__(self):
    self.sk_seed = []
    self.pk_seed = []
    self.mt = identity_matrix( gf , n )
    self.ms = identity_matrix( gf , m )
    self.t1 = Seckey.Mt1(0)
    self.t2 = Seckey.Mt2(0)
    self.t3 = Seckey.Mt3(0)
    self.s1 = Seckey.Ms1(0)

    self.l1_F1s = [ Seckey.F1(0) for i in range(o1) ]
    self.l1_F2s = [ Seckey.F2(0) for i in range(o1) ]
    self.l2_F1s = [ Seckey.F1(0) for i in range(o2) ]
    self.l2_F2s = [ Seckey.F2(0) for i in range(o2) ]
    self.l2_F3s = [ Seckey.F3(0) for i in range(o2) ]
    self.l2_F5s = [ Seckey.F5(0) for i in range(o2) ]
    self.l2_F6s = [ Seckey.F6(0) for i in range(o2) ]


###########################################################################

def eval_pubkey( pk , x ) :
  x_v1 = x[:v1]
  x_o1 = x[v1:v1+o1]
  x_o2 = x[v1+o1:v1+o1+o2]
  z = []
  for i in range(o1):
    z.append( eval_trimat(pk.l1_F1s[i],x_v1) + eval_recmat(pk.l1_F2s[i],x_v1,x_o1) + eval_recmat(pk.l1_F3s[i],x_v1,x_o2) \
       + eval_trimat(pk.l1_F5s[i],x_o1) + eval_recmat(pk.l1_F6s[i],x_o1,x_o2) + eval_trimat(pk.l1_F9s[i],x_o2) )
  for i in range(o2):
    z.append( eval_trimat(pk.l2_F1s[i],x_v1) + eval_recmat(pk.l2_F2s[i],x_v1,x_o1) + eval_recmat(pk.l2_F3s[i],x_v1,x_o2) \
       + eval_trimat(pk.l2_F5s[i],x_o1) + eval_recmat(pk.l2_F6s[i],x_o1,x_o2) + eval_trimat(pk.l2_F9s[i],x_o2) )
  return vector(z)


#############################################################################


def generate_B1_B2( prng_state , pk ):
  prng1 = prng_state
  if keyformat_column_major :
    # Q1, in the B1 upper parts
    MM = MatrixSpace( gf , n_terms_trimat(v1) , o1 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l1_F1s = macaulaymat_to_trimats( mat , v1 )

    # Q2, in the B1 upper parts
    MM = MatrixSpace( gf , v1*o1 , o1 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l1_F2s = macaulaymat_to_recmats( mat , v1 , o1 )

    # Q1, in the B1 lower parts
    MM = MatrixSpace( gf , n_terms_trimat(v1) , o2 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l2_F1s = macaulaymat_to_trimats( mat , v1 )

    # Q2, in the B1 lower parts
    MM = MatrixSpace( gf , v1*o1 , o2 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l2_F2s = macaulaymat_to_recmats( mat , v1 , o1 )

    # Q3, in the B2
    MM = MatrixSpace( gf , v1*o2 , o2 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l2_F3s = macaulaymat_to_recmats( mat , v1 , o2 )

    # Q5, in the B2
    MM = MatrixSpace( gf , n_terms_trimat(o1) , o2 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l2_F5s = macaulaymat_to_trimats( mat , o1 )

    # Q6, in the B2
    MM = MatrixSpace( gf , o1*o2 , o2 )
    tmp, prng1 = prng_gen(prng1, MM.dimension() )
    mat = MM(tmp).transpose()
    pk.l2_F6s = macaulaymat_to_recmats( mat , o1 , o2 )
  else:
    # B1 upper parts
    for i in range(o1) :
      tmp, prng1 = prng_gen(prng1, n_terms_trimat(v1) )
      pk.l1_F1s[i] = trimat(v1, tmp)
      tmp, prng1 = prng_gen(prng1, Pubkey.F2.nrows()*Pubkey.F2.ncols() )
      pk.l1_F2s[i] = Pubkey.F2(tmp)
    for i in range(o2) :
      # B1 lower parts
      tmp, prng1 = prng_gen(prng1, n_terms_trimat(v1) )
      pk.l2_F1s[i] = trimat(v1, tmp)
      tmp, prng1 = prng_gen(prng1, Pubkey.F2.nrows()*Pubkey.F2.ncols() )
      pk.l2_F2s[i] = Pubkey.F2(tmp)
      # B2
      tmp, prng1 = prng_gen(prng1, Pubkey.F3.nrows()*Pubkey.F3.ncols() )
      pk.l2_F3s[i] = Pubkey.F3(tmp)
      tmp, prng1 = prng_gen(prng1, n_terms_trimat(o1) )
      pk.l2_F5s[i] = trimat(o1, tmp)
      tmp, prng1 = prng_gen(prng1, Pubkey.F6.nrows()*Pubkey.F6.ncols() )
      pk.l2_F6s[i] = Pubkey.F6(tmp)
  return pk, prng1




def generate_S_T( prng_state , sk ):
  prng0 = prng_state
  if keyformat_column_major :
    # S map
    tmp, prng0 = prng_gen(prng0, Seckey.Ms1.nrows()*Seckey.Ms1.ncols() )
    sk.s1 = MatrixSpace( gf , Seckey.Ms1.ncols() , Seckey.Ms1.nrows() )(tmp).transpose()
    sk.ms.set_block( 0 , o1 , sk.s1 )

    # T map
    tmp, prng0 = prng_gen(prng0, Seckey.Mt1.nrows()*Seckey.Mt1.ncols() )
    sk.t1 = MatrixSpace( gf , Seckey.Mt1.ncols() , Seckey.Mt1.nrows() )(tmp).transpose()
    sk.mt.set_block( 0 , v1 , sk.t1 )
    tmp, prng0 = prng_gen(prng0, Seckey.Mt2.nrows()*Seckey.Mt2.ncols() )
    sk.t2 = MatrixSpace( gf , Seckey.Mt2.ncols() , Seckey.Mt2.nrows() )(tmp).transpose()
    sk.mt.set_block( 0 , v1 + o1 , sk.t2 )
    tmp, prng0 = prng_gen(prng0, Seckey.Mt3.nrows()*Seckey.Mt3.ncols() )
    sk.t3 = MatrixSpace( gf , Seckey.Mt3.ncols() , Seckey.Mt3.nrows() )(tmp).transpose()
    sk.mt.set_block( v1 , v1 + o1 , sk.t3 )
  else:
    # S map
    tmp, prng0 = prng_gen(prng0, Seckey.Ms1.nrows()*Seckey.Ms1.ncols() )
    sk.s1 = Seckey.Ms1(tmp)
    sk.ms.set_block( 0 , o1 , sk.s1 )

    # T map
    tmp, prng0 = prng_gen(prng0, Seckey.Mt1.nrows()*Seckey.Mt1.ncols() )
    sk.t1 = Seckey.Mt1(tmp)
    sk.mt.set_block( 0 , v1 , sk.t1 )
    tmp, prng0 = prng_gen(prng0, Seckey.Mt2.nrows()*Seckey.Mt2.ncols() )
    sk.t2 = Seckey.Mt2(tmp)
    sk.mt.set_block( 0 , v1 + o1 , sk.t2 )
    tmp, prng0 = prng_gen(prng0, Seckey.Mt3.nrows()*Seckey.Mt3.ncols() )
    sk.t3 = Seckey.Mt3(tmp)
    sk.mt.set_block( v1 , v1 + o1 , sk.t3 )
  return sk, prng0




#######################################################


def UT( mat ):
  for i in range( mat.ncols() ):
    for j in range( i+1 , mat.nrows() ):
      if i == j : continue
      mat[i,j] = mat[i,j] + mat[j,i]
      mat[j,i] = mat[j,i] - mat[j,i]
  return mat


def calculate_F_from_Q( F_sk , T_sk , Q_pk ) :
  # 2nd step, first layer
  for i in range(o1):
    F_sk.l1_F1s[i] = Q_pk.l1_F1s[i]
    F_sk.l1_F2s[i] = ( Q_pk.l1_F1s[i] + Q_pk.l1_F1s[i].transpose() ) * T_sk.t1 + Q_pk.l1_F2s[i]

  # 3rd step, second layer
  t4 = T_sk.t1 * T_sk.t3 - T_sk.t2
  t1_tr = T_sk.t1.transpose()
  for i in range(o2):
        
    F_sk.l2_F1s[i] = Q_pk.l2_F1s[i]

    Q1_T1 = Q_pk.l2_F1s[i]*T_sk.t1
    F_sk.l2_F2s[i] =              Q1_T1 + Q_pk.l2_F2s[i]     + Q_pk.l2_F1s[i].transpose() * T_sk.t1
    F_sk.l2_F5s[i] = UT( t1_tr* ( Q1_T1 + Q_pk.l2_F2s[i] ) ) + Q_pk.l2_F5s[i]

    Q1_Q1T_T4 =  (Q_pk.l2_F1s[i] + Q_pk.l2_F1s[i].transpose()) * t4
    Q2_T3 = Q_pk.l2_F2s[i]*T_sk.t3
    F_sk.l2_F3s[i] = Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i]
    F_sk.l2_F6s[i] = t1_tr * ( Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i] ) \
                    + Q_pk.l2_F2s[i].transpose() * t4 + (Q_pk.l2_F5s[i] + Q_pk.l2_F5s[i].transpose())*T_sk.t3 + Q_pk.l2_F6s[i]

    #F_sk.l2_F1s[i] = Q_pk.l2_F1s[i]
    #Q1_Q1T = (Q_pk.l2_F1s[i]+Q_pk.l2_F1s[i].transpose())
    #F_sk.l2_F2s[i] = Q1_Q1T * T_sk.t1 + Q_pk.l2_F2s[i]
    #Q1_Q1T_T4 =  Q1_Q1T * t4
    #F_sk.l2_F3s[i] = Q1_Q1T_T4 + Q_pk.l2_F2s[i]*T_sk.t3 + Q_pk.l2_F3s[i]
    #F_sk.l2_F5s[i] = UT( t1_tr*Q_pk.l2_F1s[i]*T_sk.t1 + t1_tr*Q_pk.l2_F2s[i] + Q_pk.l2_F5s[i] )
    #F_sk.l2_F6s[i] = t1_tr * ( Q1_Q1T_T4 + Q_pk.l2_F2s[i] * T_sk.t3 + Q_pk.l2_F3s[i] ) \
    #                + Q_pk.l2_F2s[i].transpose() * t4 + (Q_pk.l2_F5s[i] + Q_pk.l2_F5s[i].transpose())*T_sk.t3 + Q_pk.l2_F6s[i]
  return F_sk


def calculate_Q_from_F( Q_pk , T_sk , F_sk ) :
  # 1st layer
  for i in range(o1):
    Q_pk.l1_F1s[i] = F_sk.l1_F1s[i]
    F1_F1T = F_sk.l1_F1s[i] + F_sk.l1_F1s[i].transpose()
    Q_pk.l1_F2s[i] = F1_F1T * T_sk.t1 + F_sk.l1_F2s[i]

    Q_pk.l1_F3s[i] = F1_F1T * T_sk.t2 + F_sk.l1_F2s[i] * T_sk.t3
    T1tr = T_sk.t1.transpose()
    Q_pk.l1_F5s[i] = UT( T1tr* (F_sk.l1_F1s[i]*T_sk.t1 + F_sk.l1_F2s[i]) )
    F2_T3 = F_sk.l1_F2s[i]*T_sk.t3
    Q_pk.l1_F6s[i] = T1tr* ( F1_F1T * T_sk.t2 + F2_T3 ) + F_sk.l1_F2s[i].transpose() * T_sk.t2
    Q_pk.l1_F9s[i] = UT( T_sk.t2.transpose()*( F_sk.l1_F1s[i]*T_sk.t2 + F2_T3 ) )

  # 2nd layer
  for i in range(o2):
    Q_pk.l2_F1s[i] = F_sk.l2_F1s[i]
    F1_F1T = F_sk.l2_F1s[i] + F_sk.l2_F1s[i].transpose()
    Q_pk.l2_F2s[i] = F1_F1T * T_sk.t1 + F_sk.l2_F2s[i]

    Q_pk.l2_F3s[i] = F1_F1T * T_sk.t2 + F_sk.l2_F2s[i] * T_sk.t3 + F_sk.l2_F3s[i]
    T1tr = T_sk.t1.transpose()

    Q_pk.l2_F5s[i] = UT( T1tr* (F_sk.l2_F1s[i]*T_sk.t1 + F_sk.l2_F2s[i]) + F_sk.l2_F5s[i] )
    F2_T3 = F_sk.l2_F2s[i]*T_sk.t3
    Q_pk.l2_F6s[i] = T1tr* ( F1_F1T * T_sk.t2 + F2_T3 + F_sk.l2_F3s[i] ) + F_sk.l2_F2s[i].transpose() * T_sk.t2 \
                     + ( F_sk.l2_F5s[i] + F_sk.l2_F5s[i].transpose() ) * T_sk.t3 + F_sk.l2_F6s[i]
    Q_pk.l2_F9s[i] = UT( T_sk.t2.transpose()*( F_sk.l2_F1s[i]*T_sk.t2 + F2_T3 + F_sk.l2_F3s[i] )   \
                      +  T_sk.t3.transpose()*( F_sk.l2_F5s[i]*T_sk.t3 + F_sk.l2_F6s[i] )  )
  return Q_pk


def calculate_Qcyclic_from_F( Q_pk , T_sk , F_sk ) :
  # 1st layer
  for i in range(o1):
    F1_F1T = F_sk.l1_F1s[i] + F_sk.l1_F1s[i].transpose()
    Q_pk.l1_F3s[i] = F1_F1T * T_sk.t2 + F_sk.l1_F2s[i] * T_sk.t3
    T1tr = T_sk.t1.transpose()
    Q_pk.l1_F5s[i] = UT( T1tr* (F_sk.l1_F1s[i]*T_sk.t1 + F_sk.l1_F2s[i]) )
    F2_T3 = F_sk.l1_F2s[i]*T_sk.t3
    Q_pk.l1_F6s[i] = T1tr* ( F1_F1T * T_sk.t2 + F2_T3 ) + F_sk.l1_F2s[i].transpose() * T_sk.t2
    Q_pk.l1_F9s[i] = UT( T_sk.t2.transpose()*( F_sk.l1_F1s[i]*T_sk.t2 + F2_T3 ) )

  # 2nd layer
  for i in range(o2):
    F2_T3 = F_sk.l2_F2s[i]*T_sk.t3
    Q_pk.l2_F9s[i] = UT( T_sk.t2.transpose()*( F_sk.l2_F1s[i]*T_sk.t2 + F2_T3 + F_sk.l2_F3s[i] )   \
                      +  T_sk.t3.transpose()*( F_sk.l2_F5s[i]*T_sk.t3 + F_sk.l2_F6s[i] )  )
  return Q_pk



def obsfucate_l1_polys( l1_polys , mat_s1 , l2_polys ):
  for i in range(mat_s1.nrows()):
    ri = mat_s1[i]
    for j in range(len(ri)):
      l1_polys[i] = l1_polys[i] + ri[j]*l2_polys[j]
  return l1_polys



#############################################################################


def seed_to_seckey_classic( gf_seed ):
  prng0 = prng_set( gf_seed )
  sk = Seckey()
  sk.sk_seed = list(gf_seed)

  # S, T map
  sk, prng0 = generate_S_T( prng0 , sk )
  # F map
  sk, prng0 = generate_B1_B2( prng0 , sk )
  return sk



# cyclic version

def seed_to_seckey( gf_seed , pk_seed ):
  prng0 = prng_set( gf_seed )
  sk = Seckey()
  sk.sk_seed = list( gf_seed )
  sk.pk_seed = list( pk_seed )
  # S, T map
  sk, prng0 = generate_S_T( prng0 , sk )

  # B1, B2
  prng1 = prng_set( pk_seed )
  pk = Pubkey()
  pk, prng1 = generate_B1_B2( prng1 , pk )

  # 1st step, generating Q(1,1) and Q(2,1)  from B1, B2
  obsfucate_l1_polys( pk.l1_F1s , sk.s1 , pk.l2_F1s )   # Q(1,1)
  obsfucate_l1_polys( pk.l1_F2s , sk.s1 , pk.l2_F2s )   # Q(1,1)
  # Q(2,1) = B1(2,1)
  # Q(2,2) = B2

  sk = calculate_F_from_Q( sk , sk , pk )

  return sk



##############################################################################



def seed_to_pubkey(sk_seed,pk_seed):
  prng1 = prng_set( pk_seed )
  pk = Pubkey()
  pk, prng1 = generate_B1_B2( prng1 , pk )

  sk = seed_to_seckey(sk_seed,pk_seed)

  pk = calculate_Qcyclic_from_F( pk , sk , sk )

  obsfucate_l1_polys( pk.l1_F3s , sk.s1 , pk.l2_F3s )
  obsfucate_l1_polys( pk.l1_F5s , sk.s1 , pk.l2_F5s )
  obsfucate_l1_polys( pk.l1_F6s , sk.s1 , pk.l2_F6s )
  obsfucate_l1_polys( pk.l1_F9s , sk.s1 , pk.l2_F9s )

  return pk


def seed_to_pubkey_classic(sk_seed):

  sk = seed_to_seckey_classic(sk_seed)
  pk = Pubkey()
  pk = calculate_Q_from_F( pk , sk , sk )

  obsfucate_l1_polys( pk.l1_F1s , sk.s1 , pk.l2_F1s )
  obsfucate_l1_polys( pk.l1_F2s , sk.s1 , pk.l2_F2s )
  obsfucate_l1_polys( pk.l1_F3s , sk.s1 , pk.l2_F3s )
  obsfucate_l1_polys( pk.l1_F5s , sk.s1 , pk.l2_F5s )
  obsfucate_l1_polys( pk.l1_F6s , sk.s1 , pk.l2_F6s )
  obsfucate_l1_polys( pk.l1_F9s , sk.s1 , pk.l2_F9s )

  return pk




################################################################################
### something for central map

def gen_o_mat( vo_maps , vinegar ):
  ret = []
  for i in range( len(vo_maps) ) :
    ret.append( vinegar*vo_maps[i] )
  return matrix(ret)

def central_map( seckey , x ):
  x_v1 = x[:v1]
  x_o1 = x[v1:v1+o1]
  x_o2 = x[v1+o1:v1+o1+o2]
  r1 = vector( [ eval_trimat( tri_mat , x_v1) for tri_mat in seckey.l1_F1s ]  ) \
     + vector( [ eval_recmat( rec_mat , x_v1 , x_o1 ) for rec_mat in seckey.l1_F2s ]  )
  r2 = vector( [ eval_trimat( tri_mat , x_v1) for tri_mat in seckey.l2_F1s ]  )         \
     + vector( [ eval_recmat( rec_mat , x_v1 , x_o1 ) for rec_mat in seckey.l2_F2s ]  ) \
     + vector( [ eval_recmat( rec_mat , x_v1 , x_o2 ) for rec_mat in seckey.l2_F3s ]  ) \
     + vector( [ eval_trimat( tri_mat , x_o1) for tri_mat in seckey.l2_F5s ] )          \
     + vector( [ eval_recmat( rec_mat , x_o1 , x_o2 ) for rec_mat in seckey.l2_F6s ]  )
  return vector( list(r1) + list(r2) )

def ivs_central_map( seckey , y , x_v1 ):
  omat_l1 = gen_o_mat(seckey.l1_F2s , x_v1)
  if omat_l1.is_singular() : return (False,vector([gf(0) for i in range(v2+o2)]))
  x_o1 = (omat_l1^-1) * ( y[:o1] + vector( [ eval_trimat( tri_mat , x_v1) for tri_mat in seckey.l1_F1s ]  ) )  

  omat_l2 = gen_o_mat(seckey.l2_F3s , x_v1) + gen_o_mat(seckey.l2_F6s , x_o1)
  if omat_l2.is_singular() : return (False,vector([gf(0) for i in range(v2+o2)]))
  x_o2 = (omat_l2^-1) * ( y[o1:o1+o2] + vector( [ eval_trimat( tri_mat , x_v1) for tri_mat in seckey.l2_F1s ]  )         \
                                      + vector( [ eval_recmat( rec_mat , x_v1 , x_o1 ) for rec_mat in seckey.l2_F2s ]  ) \
                                      + vector( [ eval_trimat( tri_mat , x_o1) for tri_mat in seckey.l2_F5s ]  ) )  
  return True,vector( list(x_v1) + list(x_o1) + list(x_o2))




#########################################################################


def _do_rainbow_verify( pubkey , dig , sig , salt ):
  ck0 = gf_hash( list(dig) + list(salt) )
  ck1 = eval_pubkey( pubkey , vector(sig) )
  return vector(ck0) == vector(ck1)


def rainbow_verify( pubkey , dig , sig ):
  gf_dig = bytes_to_gfs( dig )
  gf_sig = bytes_to_gfs( sig[:(n//2)] )
  gf_salt = bytes_to_gfs( sig[(n//2):] )
  return _do_rainbow_verify( pubkey , gf_dig , gf_sig , gf_salt )



### for debug

def pubmap_with_seckey( seckey , _w ):
  w = vector(_w)
  x = seckey.mt * w
  y = central_map( seckey , x )
  z = seckey.ms * y
  return z



def _do_rainbow_sign( seckey , digest ):
  #seckey = seed_to_seckey(sec_seed, pk_seed )
  seed = gf_hash( list(seckey.sk_seed) + list(seckey.pk_seed) + list(digest) )
  prng_state = prng_set(seed)

  ### roll vinegar
  r_rng, prng_state = prng_gen( prng_state , v1 )
  v_l1 = vector(r_rng)
  mo1 = gen_o_mat(seckey.l1_F2s , v_l1)
  while( mo1.is_singular() ):
    r_rng, prng_state = prng_gen( prng_state , v1 )
    v_l1 = vector(r_rng)
    mo1 = gen_o_mat(seckey.l1_F2s , v_l1)

  ### main work start
  r = False
  time = 0
  while not r :
    # roll salt
    gfsalt , prng_state = prng_gen( prng_state , len_gfsalt )
    digest_salt = vector( list(digest) + list(gfsalt) )
    z = vector( gf_hash( digest_salt )[:m] )

    y = ( seckey.ms^-1 ) * z
    r,x = ivs_central_map( seckey , y , v_l1 )
    w  = ( seckey.mt^-1 ) * x

    time = time + 1
    if time >= 128 : break
  return w,vector(gfsalt),r



def rainbow_sign( keyseed , digest ):
  sk_seed = gf_hash( bytes_to_gfs( keyseed[0] ) )
  pk_seed = gf_hash( bytes_to_gfs( keyseed[1] ) )
  seckey = seed_to_seckey(sec_seed, pk_seed )
  gf_dig = bytes_to_gfs( digest )
  gf_sig , gf_salt , r = _do_rainbow_sign( seckey , gf_dig )
  return r , gfs_to_bytes( list(gf_sig) + list(gf_salt) )



def rainbow_sign_classic( sk_seed , digest ):
  #sk_seed = gf_hash( bytes_to_gfs( sk_seed ) )
  sk_seed = bytes_to_gfs( sk_seed )
  seckey = seed_to_seckey_classic(sk_seed )
  gf_dig = bytes_to_gfs( digest )
  gf_sig , gf_salt , r = _do_rainbow_sign( seckey , gf_dig )
  return r , gfs_to_bytes( list(gf_sig) + list(gf_salt) )







############################
## testing
############################


# 0: set key and digest

key = [ binascii.unhexlify( 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC501A00000000000020080000' ) , binascii.unhexlify( 'EF'*16 ) ]

sk_seed = gf_hash( bytes_to_gfs( key[0] ) )
pk_seed = gf_hash( bytes_to_gfs( key[1] ) )

print "sk seed:", binascii.hexlify( gfs_to_bytes(sk_seed) )
print "pk seed:", binascii.hexlify( gfs_to_bytes(pk_seed) )


# TEST 1 from new-impl-sk/test.c
raw_dig = binascii.unhexlify('e75b10130b0f1d4d81d7ef0d02c3d8a4f83c2a48ef34cdf16f703b1d2a21f422') #b'\x00\x01\x02\x03'*8
dig = bytes_to_gfs( raw_dig )
print "digest:", dig


print "generating key paris...."

sk = seed_to_seckey( sk_seed , pk_seed )

pk = seed_to_pubkey( sk_seed , pk_seed )


print "keys ready."


# TEST 0 self sign/verify test
# private sign/verify functions
sig, salt, r = _do_rainbow_sign( sk_seed , pk_seed , dig )
print "sig,salt,r:", gf_hexlify( sig) , gf_hexlify(salt) , r

z0 = gf_hash( list(dig) + list(salt) )
print "hash of (dig+salt) ->"
print "cp0:", gf_hexlify( z0 )

z1 = pubmap_with_seckey( sk , sig )
print "verify with seckey:"
print "cp1:", gf_hexlify( z1 )
print "verified? ", vector(z0) == vector(z1)

z2 = eval_pubkey( pk , sig )
print "verify with pubkey:"
print "cp2:", gf_hexlify( z2 )
print "verified? ", vector(z0) == vector(z2)


# comparing results of private functions to the public signing function.
r, byte_sig = rainbow_sign( key , raw_dig )
print r
sig2 = vector(bytes_to_gfs( byte_sig[:(n//2)] ) )
salt2 = vector(bytes_to_gfs( byte_sig[(n//2):] ) )
print "sig eq?", sig == sig2
print "salt eq?", salt == salt2
print "full signature:", binascii.hexlify(byte_sig)


################################################################################
# TEST for generating keys with seed "0"
################################################################################

sk_seed1 = [ gf(0) for i in range(64) ]
pk_seed1 = [ gf(0) for i in range(64) ]

print "sk seed1:", gf_hexlify(sk_seed1)
print "pk seed1:", gf_hexlify(pk_seed1)


print "generating key paris...."

sk1 = seed_to_seckey( sk_seed1 , pk_seed1 )
pk1 = seed_to_pubkey( sk_seed1 , pk_seed1 )

print "keys ready."

sk_seed2_raw = binascii.unhexlify( '00'*32 )
sk_seed2 = bytes_to_gfs( sk_seed2_raw )
sk2 = seed_to_seckey_classic( sk_seed2 )
pk2 = seed_to_pubkey_classic( sk_seed2 )

dig = sha256(binascii.unhexlify( '00'*32 ))
print "digest:", binascii.hexlify( dig )
gf_dig = bytes_to_gfs( dig )
rr = _do_rainbow_sign( sk2 , gf_dig )
print "sig: ", rr[2] , ":" , gf_hexlify( list(rr[0]) + list(rr[1]) )


################################################################################


print "sk1.l1_F1", gf_hexlify( trimats_to_macaulaymat_cols( sk1.l1_F1s )[0] )
print "sk1.l1_F2", gf_hexlify( recmats_to_macaulaymat_cols( sk1.l1_F2s)[0] )
print "sk1.l1_F2", gf_hexlify( recmats_to_macaulaymat_cols( sk1.l1_F2s)[1] )

print "sk1.l1_F3", gf_hexlify( recmats_to_macaulaymat_cols( sk1.l2_F3s)[0] )
print "sk1.l1_F3", gf_hexlify( recmats_to_macaulaymat_cols( sk1.l2_F3s)[1] )

print "sk1.l2_F5", gf_hexlify( trimats_to_macaulaymat_cols( sk1.l2_F5s )[0] )
print "sk1.l2_F5", gf_hexlify( trimats_to_macaulaymat_cols( sk1.l2_F5s )[1] )

print "sk1.l2_F6", gf_hexlify( recmats_to_macaulaymat_cols( sk1.l2_F6s)[0] )
print "sk1.l2_F6", gf_hexlify( recmats_to_macaulaymat_cols( sk1.l2_F6s)[1] )


print "public map evaluations:"

ii = '0'*n
print "eval", ii
print gf_hexlify( eval_pubkey( pk1 , hex_to_gfs( ii ) ) )
print gf_hexlify( pubmap_with_seckey( sk1 , hex_to_gfs( ii ) ) )

ii = '1'*n
print "eval", ii
print gf_hexlify( eval_pubkey( pk1 , hex_to_gfs( ii ) ) )
print gf_hexlify( pubmap_with_seckey( sk1 , hex_to_gfs( ii ) ) )

ii = 'ef'*(n//2)
print "eval", ii
print gf_hexlify( eval_pubkey( pk1 , hex_to_gfs( ii ) ) )
print gf_hexlify( pubmap_with_seckey( sk1 , hex_to_gfs( ii ) ) )


print "generating classic keys"
sk2 = seed_to_seckey_classic( sk_seed1  )
pk2 = seed_to_pubkey_classic( sk_seed1  )
print "keys ready."


ii = '0'*n
print "eval", ii
print gf_hexlify( eval_pubkey( pk2 , hex_to_gfs( ii ) ) )
print gf_hexlify( pubmap_with_seckey( sk2 , hex_to_gfs( ii ) ) )

ii = '1'*n
print "eval", ii
print gf_hexlify( eval_pubkey( pk2 , hex_to_gfs( ii ) ) )
print gf_hexlify( pubmap_with_seckey( sk2 , hex_to_gfs( ii ) ) )

ii = 'ef'*(n//2)
print "eval", ii
print gf_hexlify( eval_pubkey( pk2 , hex_to_gfs( ii ) ) )
print gf_hexlify( pubmap_with_seckey( sk2 , hex_to_gfs( ii ) ) )




# TEST 1 from new-impl-sk/test.c
# comparing the signature to the result of C code
# assert binascii.hexlify(byte_sig) == '03b81c34712a28578166a3ddc09814b0cc8121130b6566727dcbdf8a855d905ba6a083ac3e8bbfb0b4f4ecb106c375aa26d1bc46842f92e152a4ab64d8dede56'

# TEST 2 public keys
# VERY  slooooooooooooooooooooooow
# pk = rainbow_gen_pubkey( key )
# print "pk[:16]:" , binascii.hexlify( pk[:16] )

