#!/bin/bash

echo "***** WARNING: check defining _SUPERCOP_ in rng.h *****"

if test -d "crypto_sign"; then
  echo "crypto_sign exist."
  exit -1
fi

echo "**** generate nist project ****"
python gen_proj2.py supercop

echo "**** generate supercop project ****"
mkdir crypto_sign
mkdir crypto_sign/rainbow1aclassic363232
mkdir crypto_sign/rainbow1acyclicc363232
mkdir crypto_sign/rainbow1acompres363232
mkdir crypto_sign/rainbow3cclassic683248
mkdir crypto_sign/rainbow3ccyclicc683248
mkdir crypto_sign/rainbow3ccompres683248
mkdir crypto_sign/rainbow5cclassic963664
mkdir crypto_sign/rainbow5ccyclicc963664
mkdir crypto_sign/rainbow5ccompres963664

cp -r supercop/Reference_Implementation/Ia_Classic            crypto_sign/rainbow1aclassic363232/ref
cp -r supercop/Reference_Implementation/Ia_Circumzenithal             crypto_sign/rainbow1acyclicc363232/ref
cp -r supercop/Reference_Implementation/Ia_Compressed   crypto_sign/rainbow1acompres363232/ref
cp -r supercop/Reference_Implementation/IIIc_Classic          crypto_sign/rainbow3cclassic683248/ref
cp -r supercop/Reference_Implementation/IIIc_Circumzenithal           crypto_sign/rainbow3ccyclicc683248/ref
cp -r supercop/Reference_Implementation/IIIc_Compressed crypto_sign/rainbow3ccompres683248/ref
cp -r supercop/Reference_Implementation/Vc_Classic            crypto_sign/rainbow5cclassic963664/ref
cp -r supercop/Reference_Implementation/Vc_Circumzenithal             crypto_sign/rainbow5ccyclicc963664/ref
cp -r supercop/Reference_Implementation/Vc_Compressed   crypto_sign/rainbow5ccompres963664/ref

cp -r supercop/Optimized_Implementation/amd64/Ia_Classic            crypto_sign/rainbow1aclassic363232/amd64
cp -r supercop/Optimized_Implementation/amd64/Ia_Circumzenithal             crypto_sign/rainbow1acyclicc363232/amd64
cp -r supercop/Optimized_Implementation/amd64/Ia_Compressed   crypto_sign/rainbow1acompres363232/amd64
cp -r supercop/Optimized_Implementation/amd64/IIIc_Classic          crypto_sign/rainbow3cclassic683248/amd64
cp -r supercop/Optimized_Implementation/amd64/IIIc_Circumzenithal           crypto_sign/rainbow3ccyclicc683248/amd64
cp -r supercop/Optimized_Implementation/amd64/IIIc_Compressed crypto_sign/rainbow3ccompres683248/amd64
cp -r supercop/Optimized_Implementation/amd64/Vc_Classic            crypto_sign/rainbow5cclassic963664/amd64
cp -r supercop/Optimized_Implementation/amd64/Vc_Circumzenithal             crypto_sign/rainbow5ccyclicc963664/amd64
cp -r supercop/Optimized_Implementation/amd64/Vc_Compressed   crypto_sign/rainbow5ccompres963664/amd64

cp -r supercop/Alternative_Implementation/ssse3/Ia_Classic            crypto_sign/rainbow1aclassic363232/ssse3
cp -r supercop/Alternative_Implementation/ssse3/Ia_Circumzenithal             crypto_sign/rainbow1acyclicc363232/ssse3
cp -r supercop/Alternative_Implementation/ssse3/Ia_Compressed   crypto_sign/rainbow1acompres363232/ssse3
cp -r supercop/Alternative_Implementation/ssse3/IIIc_Classic          crypto_sign/rainbow3cclassic683248/ssse3
cp -r supercop/Alternative_Implementation/ssse3/IIIc_Circumzenithal           crypto_sign/rainbow3ccyclicc683248/ssse3
cp -r supercop/Alternative_Implementation/ssse3/IIIc_Compressed crypto_sign/rainbow3ccompres683248/ssse3
cp -r supercop/Alternative_Implementation/ssse3/Vc_Classic            crypto_sign/rainbow5cclassic963664/ssse3
cp -r supercop/Alternative_Implementation/ssse3/Vc_Circumzenithal             crypto_sign/rainbow5ccyclicc963664/ssse3
cp -r supercop/Alternative_Implementation/ssse3/Vc_Compressed   crypto_sign/rainbow5ccompres963664/ssse3

cp -r supercop/Alternative_Implementation/avx2/Ia_Classic            crypto_sign/rainbow1aclassic363232/avx2
cp -r supercop/Alternative_Implementation/avx2/Ia_Circumzenithal             crypto_sign/rainbow1acyclicc363232/avx2
cp -r supercop/Alternative_Implementation/avx2/Ia_Compressed   crypto_sign/rainbow1acompres363232/avx2
cp -r supercop/Alternative_Implementation/avx2/IIIc_Classic          crypto_sign/rainbow3cclassic683248/avx2
cp -r supercop/Alternative_Implementation/avx2/IIIc_Circumzenithal           crypto_sign/rainbow3ccyclicc683248/avx2
cp -r supercop/Alternative_Implementation/avx2/IIIc_Compressed crypto_sign/rainbow3ccompres683248/avx2
cp -r supercop/Alternative_Implementation/avx2/Vc_Classic            crypto_sign/rainbow5cclassic963664/avx2
cp -r supercop/Alternative_Implementation/avx2/Vc_Circumzenithal             crypto_sign/rainbow5ccyclicc963664/avx2
cp -r supercop/Alternative_Implementation/avx2/Vc_Compressed   crypto_sign/rainbow5ccompres963664/avx2

cp architectures crypto_sign/rainbow1aclassic363232/amd64/
cp architectures crypto_sign/rainbow1aclassic363232/ssse3/
cp architectures crypto_sign/rainbow1aclassic363232/avx2/
cp architectures crypto_sign/rainbow1acyclicc363232/amd64/
cp architectures crypto_sign/rainbow1acyclicc363232/ssse3/
cp architectures crypto_sign/rainbow1acyclicc363232/avx2/
cp architectures crypto_sign/rainbow1acompres363232/amd64/
cp architectures crypto_sign/rainbow1acompres363232/ssse3/
cp architectures crypto_sign/rainbow1acompres363232/avx2/

cp architectures crypto_sign/rainbow3cclassic683248/amd64/
cp architectures crypto_sign/rainbow3cclassic683248/ssse3/
cp architectures crypto_sign/rainbow3cclassic683248/avx2/
cp architectures crypto_sign/rainbow3ccyclicc683248/amd64/
cp architectures crypto_sign/rainbow3ccyclicc683248/ssse3/
cp architectures crypto_sign/rainbow3ccyclicc683248/avx2/
cp architectures crypto_sign/rainbow3ccompres683248/amd64/
cp architectures crypto_sign/rainbow3ccompres683248/ssse3/
cp architectures crypto_sign/rainbow3ccompres683248/avx2/

cp architectures crypto_sign/rainbow5cclassic963664/amd64/
cp architectures crypto_sign/rainbow5cclassic963664/ssse3/
cp architectures crypto_sign/rainbow5cclassic963664/avx2/
cp architectures crypto_sign/rainbow5ccyclicc963664/amd64/
cp architectures crypto_sign/rainbow5ccyclicc963664/ssse3/
cp architectures crypto_sign/rainbow5ccyclicc963664/avx2/
cp architectures crypto_sign/rainbow5ccompres963664/amd64/
cp architectures crypto_sign/rainbow5ccompres963664/ssse3/
cp architectures crypto_sign/rainbow5ccompres963664/avx2/

echo "**** generate crypto_core ****"

python cp_crypto_core.py

echo "**** generate tar ball ****"

tar -czvf rainbow-nist-r3.tar.gz crypto_sign crypto_core

echo "**** clean ****"
rm -r supercop
rm -r crypto_sign
rm -r crypto_core
