#!/usr/bin/python



def mkDictionaryFromFile( file_name ):
  l = list()
  try:
    l = list( open( file_name ) )
  except:
    print( 'fail to open file: ' + file_name )
  c = [ i.split("=") for i in l ]
  d = { i[0].strip():i[1].strip() for i in c if len(i)>1 }
  return d



def modifyFile( file_name , dic ):
  try:
    content = list( open( file_name ) )
  except:
    print( 'fail to open file: ' + file_name )
    return
  try:
    fp = open( file_name , "w" )
  except :
    return
  for l in content:
    try:
      val = dic[l.strip()]
      fp.write( l.strip() + '\t\t-- ' + val + '\n' )
    except:
      fp.write( l )
  fp.close()
  print(file_name + ' is modified.')


dd = mkDictionaryFromFile( 'file_descriptions' )

import sys
if 2 != len(sys.argv) :
  print("Usage: exe file_name_to_be_modified\n\n")
  sys.exit(-1)

modifyFile( sys.argv[1] , dd )
