#!/usr/bin/python

import sys
import os, errno, shutil
from shutil import copyfile

base_dir_name = './submissions'

if 2 == len(sys.argv) :
  base_dir_name = sys.argv[1]
  print "base_dir_name: ", base_dir_name


dest_proj_prefix = [ '/Ia' , '/IIIc' , '/Vc' ]
dest_proj_suffix = [ '_Classic' , '_Circumzenithal' , '_Compressed' ]

imple_dir_names = [ '/Reference_Implementation' , '/Optimized_Implementation/amd64' , '/Alternative_Implementation/ssse3' , '/Alternative_Implementation/avx2' ]

def make_dirs( base_dir_name , sub_dir_names ):
    try:
        for i in sub_dir_names :
          p2 = base_dir_name + '/' + i
          print( 'makedirs(: ' + p2 + ')' )
          os.makedirs(p2)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def mk_submission_dirs():
  make_dirs( './' , [base_dir_name] )
  make_dirs( base_dir_name , imple_dir_names )
  for d in imple_dir_names :
    b = base_dir_name + d
    make_dirs( b , [ i + j for i in dest_proj_prefix for j in dest_proj_suffix ] )


mk_submission_dirs()

#
# copy files
#

def copy_files_from_one_dir( dest_dir , src_dir ):
    src_files = os.listdir(src_dir)
    for file_name in src_files:
        full_file_name = os.path.join(src_dir, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy2(full_file_name, dest_dir)
            print('copy files: ' + src_dir + '/ to ' + dest_dir + '/' )

def copy_files( dest_dir , src_dirs ):
  for s in src_dirs:
    copy_files_from_one_dir( dest_dir , s )


src_dirs = [ 'src' , 'utils' ]
imple_dirs = [ 'ref' , 'amd64' , 'ssse3' , 'avx2' ]

#base_dir_name = './submissions'
#imple_dir_names = [ '/Reference_Implementation' , '/Optimized_Implementation/amd64' , '/Optimized_Implementation/avx2' ]

# copy different files based on the name of imple dirs.
def copy_imple_files():
  for d in imple_dir_names :
    b = base_dir_name + d
    impl_idx = imple_dir_names.index(d)
    dests = [ i + j for i in dest_proj_prefix for j in dest_proj_suffix ]
    for dd in dests :
      copy_files( b + dd , src_dirs )
      copy_files( b + dd , [ imple_dirs[impl_idx] ]  )


copy_imple_files()

#
#  Copy extra fiels
#
def copy_extra_files():
  for d in imple_dir_names :
    b = base_dir_name + d
    copy_files( b , [ 'extra_files' ] )

copy_extra_files()


#
#  modify files
#

def find_1st_match_line( file_name , phrase ):
  try:
    content = list( open( file_name ) )
  except :
    raise
  for i in range( len(content) ) :
    if content[i].startswith( phrase ) :
      return i
  return -1


def file_change_one_line( file_name , line_idx , new_content ):
  try:
    content = list( open( file_name ) )
  except :
    pass
  if len( content ) <= line_idx :
    return
  print( "chage line " + str(line_idx) + " in " + file_name + " -> " + new_content )
  try:
    fp = open( file_name , "w" )
  except :
    return
  for i in range(len(content)):
    if line_idx == i :
      fp.write( new_content + '\n' )
    else:
      fp.write( content[i] )
  fp.close()





#base_dir_name = './submissions'
#imple_dir_names = [ '/Reference_Implementation' , '/Optimized_Implementation/amd64' , '/Optimized_Implementation/avx2' ]

config_file_names = [ 'rainbow_config.h' , 'api_config.h' ]
define_marker = '#define _RAINBOW'
define_paras = [ '#define _RAINBOW16_36_32_32' , '#define _RAINBOW256_68_32_48' , '#define _RAINBOW256_96_36_64' ]
define_apis = [ '#define _RAINBOW_CLASSIC' , '#define _RAINBOW_CIRCUMZENITHAL' , '#define _RAINBOW_COMPRESSED' ]


# based on the name of
#  dest_proj_prefix = [ '/Ia' , '/IIIc' , '/Vc' ]  , and
#  dest_proj_suffix = [ '_Classic' , '_Cyclic' , '_CompressedCyclic' ]
# , change the contents of config files.

for d in imple_dir_names :
  b = base_dir_name + d
  for i in dest_proj_prefix :
    idx_para = dest_proj_prefix.index(i)
    for j in dest_proj_suffix :
      idx_api = dest_proj_suffix.index(j)
      dest = b + i + j

      line_cnt = find_1st_match_line( dest + '/' + config_file_names[0] , define_marker )
      file_change_one_line( dest + '/' + config_file_names[0] , line_cnt , define_paras[idx_para] )

      line_cnt = find_1st_match_line( dest + '/' + config_file_names[1] , define_marker )
      file_change_one_line( dest + '/' + config_file_names[1] , line_cnt , define_apis[idx_api] )

      if('supercop' == base_dir_name ) : file_change_one_line( dest+'/rng.h' , find_1st_match_line(dest+'/rng.h','//#define _SUPERCOP_') , '#define _SUPERCOP_' )





for d in [ '/Reference_Implementation' , '/Optimized_Implementation/amd64' ] :
  b = base_dir_name + d
  for i in dest_proj_prefix :
    idx_para = dest_proj_prefix.index(i)
    for j in dest_proj_suffix :
      idx_api = dest_proj_suffix.index(j)
      dest = b + i + j

      line_cnt = find_1st_match_line( dest + '/' + 'utils_malloc.h' , '#define _HAS_ALIGNED_ALLOC_' )
      file_change_one_line( dest + '/' + 'utils_malloc.h' , line_cnt , '//#define _HAS_ALIGNED_ALLOC_' )



