
#include <stdio.h>

#include "rainbow_keypair.h"


#include "blas.h"
#include "utils.h"

#include "utils_prng.h"


const unsigned char * set_hexstring( unsigned char * res , const char * str , unsigned len )
{
  char byte[4] = {0};
  while( len-- ) {
    byte[0] = str[0];
    byte[1] = str[1];
    unsigned rr;
    sscanf( byte , "%x" , &rr );
    res[0] = (unsigned char)(rr&0xff);
    res ++;
    str += 2;
  }
  return res;
}

///////////////////////////////////////////////////////////////

/// public_map.h

void public_map( unsigned char * z, const pk_t * pk, const unsigned char * w );

void public_map_cyclic( unsigned char * z, const cpk_t * pk, const unsigned char * w );


////////////////////////////////////


/// for debug:
void public_map_with_seckey( unsigned char * z , const sk_t * sk, const unsigned char * w );

void public_map_cyclic_with_seckey( unsigned char * z , const csk_t * sk, const unsigned char * w );



////////////////////////////////////////////////////
/// public_map.c
////////////////////////////////////////////////////

#include "rainbow_blas.h"
#include "stdlib.h"
#include "string.h"


void public_map( unsigned char * z, const pk_t * pk, const unsigned char * w )
{
    memset( z , 0 , _PUB_M_BYTE );
    batch_quad_trimat_eval( z , pk->pk , w , _PUB_N , _PUB_M_BYTE );
}


void public_map_cyclic( unsigned char * z, const cpk_t * cpk, const unsigned char * w )
{
    pk_t * pk = (pk_t *)aligned_alloc( 32 , sizeof(pk_t) + 32 );
    cpk_to_pk( pk , cpk );
    public_map( z , pk , w );
    free( pk );
}




////////////////////////////////////
/// for debug:
////////////////////////////////////


static
void rainbow_central_map( unsigned char * y, const sk_t * sk, const unsigned char * x )
{
    const unsigned char * v1 = x;
    const unsigned char * o1 = v1 + _V1_BYTE;
    const unsigned char * o2 = o1 + _O1_BYTE;

    unsigned char * l1 = y;
    unsigned char * l2 = l1 + _O1_BYTE;
    memset( y , 0 , _O1_BYTE + _O2_BYTE );

    unsigned char temp[_O1_BYTE+_O2_BYTE];

/// layer 1
    batch_quad_trimat_eval( temp , sk->l1_F1 , v1 , _V1 , _O1_BYTE );
    gf256v_add( l1 , temp , _O1_BYTE );
    batch_quad_recmat_eval( temp , v1 , _V1 , sk->l1_F2 , o1 , _O1 , _O1_BYTE );
    gf256v_add( l1 , temp , _O1_BYTE );

/// layer 2
    batch_quad_trimat_eval( temp , sk->l2_F1 , v1 , _V1 , _O2_BYTE );
    gf256v_add( l2 , temp , _O2_BYTE );
    batch_quad_recmat_eval( temp , v1 , _V1 , sk->l2_F2 , o1 , _O1 , _O2_BYTE );
    gf256v_add( l2 , temp , _O2_BYTE );
    batch_quad_recmat_eval( temp , v1 , _V1 , sk->l2_F3 , o2 , _O2 , _O2_BYTE );
    gf256v_add( l2 , temp , _O2_BYTE );

    batch_quad_trimat_eval( temp , sk->l2_F5 , o1 , _O1 , _O2_BYTE );
    gf256v_add( l2 , temp , _O2_BYTE );
    batch_quad_recmat_eval( temp , o1 , _O1 , sk->l2_F6 , o2 , _O2 , _O2_BYTE );
    gf256v_add( l2 , temp , _O2_BYTE );
}


static
void calculate_t4( unsigned char * t2_to_t4 , const unsigned char *t1 , const unsigned char *t3 )
{
///  t4 = T_sk.t1 * T_sk.t3 - T_sk.t2
    unsigned char * t4 = t2_to_t4;
    for(unsigned i=0;i<_O2;i++) {  /// t3 width
        for(unsigned j=0;j<_O1;j++) { /// t3 height
            gfv_madd( t4 , &t1[j*_V1_BYTE] , gfv_get_ele( t3 , j ) , _V1_BYTE );
        }
        t4 += _V1_BYTE;
        t3 += _O1_BYTE;
    }
}

void public_map_with_seckey( unsigned char * z , const sk_t * _sk, const unsigned char * w )
{

    unsigned char tt0[_PUB_N_BYTE] = {0};
    unsigned char tt1[_PUB_N_BYTE] = {0};
    unsigned char tt2[_PUB_N_BYTE] = {0};

    const unsigned char * v1 = w;
    const unsigned char * o1 = v1 + _V1_BYTE;
    const unsigned char * o2 = o1 + _O1_BYTE;

    sk_t sk_inst;
    sk_t * sk = &sk_inst;
    memcpy( sk , _sk , sizeof(sk_t) );
    calculate_t4( sk->t4 , sk->t1 , sk->t3 );  /// t2

    gf256v_add(tt0, w, _PUB_N_BYTE );  /// identity
    gfmat_prod(tt1, sk->t1, _V1_BYTE, _O1, o1);   /// t1
    gf256v_add(tt0, tt1, _V1_BYTE );
    gfmat_prod(tt1, sk->t4, _V1_BYTE, _O2, o2);   /// t2
    gf256v_add(tt0, tt1, _V1_BYTE );
    gfmat_prod(tt1, sk->t3, _O1_BYTE, _O2, o2);   /// t3
    gf256v_add(tt0 + _V1_BYTE, tt1, _O1_BYTE );

    rainbow_central_map(tt2, sk, tt0);

    memcpy(z, tt2, _PUB_M_BYTE); /// identity
    gfmat_prod(tt1, sk->s1, _O1_BYTE, _O2, z+_O1_BYTE);
    gf256v_add(z, tt1, _O1_BYTE);

}


void public_map_cyclic_with_seckey( unsigned char * z , const csk_t * csk, const unsigned char * w )
{
    sk_t sk;
    generate_secretkey_cyclic( &sk, csk->pk_seed , csk->sk_seed );
    public_map_with_seckey( z , &sk , w );
}








/////////////////////////////////////////////////////////////////


/// cyclic version
const char * tc0_inp_0c = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
const char * tc0_out_0c = "0000000000000000000000000000000000000000000000000000000000000000";

const char * tc0_inp_1c = "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
const char * tc0_out_1c = "6d1fa5bb2c8249b68f82e63a303b4edfa11307bc544e73f881d79beb10636178";

const char * tc0_inp_2c = "efefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefef";
const char * tc0_out_2c = "a34b13c31ceca52d58ab28d82eb3cd12a3c4191db1477e982fefedff3f8bc1bf";



int test_0c( const cpk_t * pk )
{
  puts("test 0: public map cyclic from sk/pk seed = {0}");
  puts("");

  unsigned char temp0[_PUB_N_BYTE];
  unsigned char temp1[_PUB_M_BYTE];
  unsigned char temp2[_PUB_M_BYTE];

  int err = 0;

  set_hexstring( temp0 , tc0_inp_0c , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE ); puts("");
  public_map_cyclic( temp1 , pk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE ); puts("");

  set_hexstring( temp2 , tc0_out_0c , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }

  set_hexstring( temp0 , tc0_inp_1c , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE );  puts("");
  public_map_cyclic( temp1 , pk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE );  puts("");

  set_hexstring( temp2 , tc0_out_1c , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }


  set_hexstring( temp0 , tc0_inp_2c , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE );  puts("");
  public_map_cyclic( temp1 , pk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE );  puts("");

  set_hexstring( temp2 , tc0_out_2c , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }

  return err;

}



const char * tc0_inp_0 = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
const char * tc0_out_0 = "0000000000000000000000000000000000000000000000000000000000000000";
const char * tc0_inp_1 = "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
const char * tc0_out_1 = "351c3c61ff1dff37cb9c675d12f0269189af1c7be04938795ef7bc6506d2702b";
const char * tc0_inp_2 = "efefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefefef";
const char * tc0_out_2 = "b06ab55da4d62d28d3f6ce942ba1920321ebdfdc00ada17d94deab750f73c8c7";


int test_0( const pk_t * pk )
{
  puts("test 0: public map from sk seed = {0}");
  puts("");

  unsigned char temp0[_PUB_N_BYTE];
  unsigned char temp1[_PUB_M_BYTE];
  unsigned char temp2[_PUB_M_BYTE];
  int err = 0;

  set_hexstring( temp0 , tc0_inp_0 , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE ); puts("");
  public_map( temp1 , pk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE ); puts("");

  set_hexstring( temp2 , tc0_out_0 , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }

  set_hexstring( temp0 , tc0_inp_1 , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE );  puts("");
  public_map( temp1 , pk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE );  puts("");

  set_hexstring( temp2 , tc0_out_1 , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }


  set_hexstring( temp0 , tc0_inp_2 , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE );  puts("");
  public_map( temp1 , pk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE );  puts("");

  set_hexstring( temp2 , tc0_out_2 , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }

  return err;

}




int test_0b( const sk_t * sk )
{
  puts("test 0b: public map with secret key from sk seed = {0}");
  puts("");

  unsigned char temp0[_PUB_N_BYTE];
  unsigned char temp1[_PUB_M_BYTE];
  unsigned char temp2[_PUB_M_BYTE];
  int err = 0;

  set_hexstring( temp0 , tc0_inp_0 , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE ); puts("");
  public_map_with_seckey( temp1 , sk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE ); puts("");

  set_hexstring( temp2 , tc0_out_0 , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }

  set_hexstring( temp0 , tc0_inp_1 , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE );  puts("");
  public_map_with_seckey( temp1 , sk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE );  puts("");

  set_hexstring( temp2 , tc0_out_1 , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }


  set_hexstring( temp0 , tc0_inp_2 , _PUB_N_BYTE );
  byte_fdump( stdout , "inp: " , temp0 , _PUB_N_BYTE );  puts("");
  public_map_with_seckey( temp1 , sk , temp0 );
  byte_fdump( stdout , "out: " , temp1 , _PUB_M_BYTE );  puts("");

  set_hexstring( temp2 , tc0_out_2 , _PUB_M_BYTE );
  gf256v_add( temp1 , temp2 , _PUB_M_BYTE );
  if( !gf256v_is_zero(temp1,_PUB_M_BYTE) ) {
    byte_fdump( stdout , "ERR.\nEXP: " , temp2 , _PUB_M_BYTE );
    puts("");
    err = -1;
  } else {
    puts(" : PASSED.");
  }

  return err;

}



////////////////////////////////////////////////////////////////////////////////



#define TEST_RUN 100


int test_1c( const cpk_t * pk , const csk_t * sk )
{
  puts("test 1: consistency of public maps for both pk/sk cyclic");
  puts("");


  unsigned char inp[_PUB_N_BYTE];
  unsigned char out_pk[_PUB_M_BYTE];
  unsigned char out_sk[_PUB_M_BYTE];

  unsigned char prng_seed[32] = {0};
  prng_t prng0;
  prng_set(&prng0, prng_seed, 32);

  for(unsigned i=0;i<TEST_RUN;i++) {
    prng_gen(&prng0, inp, _PUB_N_BYTE);

    public_map_cyclic( out_pk , pk , inp );
    public_map_cyclic_with_seckey( out_sk , sk , inp );

    unsigned char ck[_PUB_M_BYTE] = {0};
    gf256v_add( ck , out_pk , _PUB_M_BYTE );
    gf256v_add( ck , out_sk , _PUB_M_BYTE );

    if( !gf256v_is_zero(ck,_PUB_M_BYTE) ) {
      printf("ERR(%d/%d).\n", i , TEST_RUN );
      byte_fdump( stdout , "inp: " , inp , _PUB_N_BYTE ); puts("");
      byte_fdump( stdout , "out_pk: " , out_pk , _PUB_M_BYTE ); puts("");
      byte_fdump( stdout , "out_sk: " , out_sk , _PUB_M_BYTE ); puts("");

      return -1;
    }
  }




  return 0;

}



int test_1( const pk_t * pk , const sk_t * sk )
{
  puts("test 1: consistency of public maps for both pk/sk");
  puts("");


  unsigned char inp[_PUB_N_BYTE];
  unsigned char out_pk[_PUB_M_BYTE];
  unsigned char out_sk[_PUB_M_BYTE];

  unsigned char prng_seed[32] = {0};
  prng_t prng0;
  prng_set(&prng0, prng_seed, 32);

  for(unsigned i=0;i<TEST_RUN;i++) {
    prng_gen(&prng0, inp, _PUB_N_BYTE);
    public_map( out_pk , pk , inp );
    public_map_with_seckey( out_sk , sk , inp );

    unsigned char ck[_PUB_M_BYTE] = {0};
    gf256v_add( ck , out_pk , _PUB_M_BYTE );
    gf256v_add( ck , out_sk , _PUB_M_BYTE );

    if( !gf256v_is_zero(ck,_PUB_M_BYTE) ) {
      printf("ERR(%d/%d).\n", i , TEST_RUN );
      byte_fdump( stdout , "inp: " , inp , _PUB_N_BYTE ); puts("");
      byte_fdump( stdout , "out_pk: " , out_pk , _PUB_M_BYTE ); puts("");
      byte_fdump( stdout , "out_sk: " , out_sk , _PUB_M_BYTE ); puts("");

      return -1;
    }
  }

  return 0;

}




int main()
{

        printf("%s\n", _S_NAME );

        printf("sk size: %lu\n", sizeof(sk_t) );
        printf("pk size: %lu\n", sizeof(pk_t) );
        printf("csk size: %lu\n", sizeof(csk_t) );
        printf("cpk size: %lu\n", sizeof(cpk_t) );
        printf("digest size: %d\n", _PUB_M_BYTE );
        printf("signature size: %d\n\n", _PUB_N_BYTE );


	unsigned char sk_seed[LEN_SKSEED] = {0};
	unsigned char pk_seed[LEN_PKSEED] = {0};

	int err = 0;
	int eq_sage_err = 0;

	printf("\n\n============= generating key pairs.  ==============\n");

	csk_t csk;
	cpk_t cpk;
	generate_compact_keypair_cyclic( &cpk , &csk , sk_seed , pk_seed );

	sk_t sk;
	pk_t pk;
	generate_keypair( &pk , &sk , sk_seed );

#ifdef _RAINBOW16_32_32_32

        printf("comparing equality of public maps from generated keys with sage results.\n");
        printf("can only test for rainbow(16,32,32,32) and debugged PRNG(hash chains). \n");

	printf("\n\n============= test 0c: public map cyclic ==============\n");

	eq_sage_err = test_0c( &cpk );
	if( eq_sage_err ) printf("ERROR.\n");
	else printf("PASSED.\n");

	printf("\n\n============= test 0: public map ==============\n");

	eq_sage_err = test_0( &pk );
	if( eq_sage_err ) printf("ERROR.\n");
	else printf("PASSED.\n");

	printf("\n\n============= test 0b: public map with seckey ==============\n");

	eq_sage_err = test_0b( &sk );
	if( eq_sage_err ) printf("ERROR.\n");
	else printf("PASSED.\n");

#endif

	printf("\n\n============= test 1c: public map/ public map with seckey  ==============\n");

	err = test_1c( &cpk , &csk );
	if( err ) goto L_ERR;
	else printf("\nPASSED.\n");


	printf("\n\n============= test 1: public map/ public map with seckey  ==============\n");

	err = test_1( &pk , &sk );
	if( err ) goto L_ERR;
	else printf("\nPASSED.\n");

	printf("public map test success.\n");
	return 0;
L_ERR:
	printf("err: %d.\n", err );
	return -1;
}

