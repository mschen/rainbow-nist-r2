

valgrind --tool=massif --pages-as-heap=yes ./forvalgrind-genkey-test pk.txt sk.txt
valgrind --tool=massif --pages-as-heap=yes ./forvalgrind-sign-test sk.txt forvalgrind-sign-test.c > sign.txt
valgrind --tool=massif --pages-as-heap=yes ./forvalgrind-verify-test pk.txt sign.txt forvalgrind-sign-test.c

rm pk.txt sk.txt sign.txt

echo "WARNING: Check if turn on -g in gcc"
echo "To see the results. use:  ms_print massif.out.XXXX | less "
