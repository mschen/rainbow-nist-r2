
#include <stdio.h>
#include <stdlib.h>

#include "rainbow_keypair.h"

#include "blas.h"
#include "utils.h"



#include "benchmark.h"



#define TEST_RUN 100


int main()
{

        printf("%s\n", _S_NAME );

        printf("sk size: %lu\n", sizeof(sk_t) );
        printf("pk size: %lu\n", sizeof(pk_t) );
        printf("csk size: %lu\n", sizeof(csk_t) );
        printf("cpk size: %lu\n", sizeof(cpk_t) );
        printf("digest size: %d\n", _PUB_M_BYTE );
        printf("signature size: %d\n\n", _PUB_N_BYTE );


	unsigned char pk_seed[LEN_PKSEED] = {0};
	unsigned char sk_seed[LEN_SKSEED] = {0};

	benchmark bm0,bm1,bm2,bm3;

	bm_init(&bm0);
	bm_init(&bm1);
	bm_init(&bm2);
	bm_init(&bm3);


	sk_t sk;
	pk_t pk;
	sk_t sk2;
	csk_t csk;
	cpk_t cpk;

	printf("\n\n============= benchmarking gen-sec-keys() ==============\n");


	for(unsigned i=0;i<TEST_RUN;i++) {
		for(unsigned j=0;j<LEN_PKSEED;j++) pk_seed[j] = rand()&0xff;
		for(unsigned j=0;j<LEN_SKSEED;j++) sk_seed[j] = rand()&0xff;

BENCHMARK( bm0 , {
		generate_secretkey_cyclic( &sk2 , pk_seed , sk_seed );
} );
BENCHMARK( bm1 , {
		generate_compact_keypair_cyclic( &cpk, &csk , pk_seed , sk_seed );
} );


BENCHMARK( bm2 , {
		generate_secretkey( &sk , sk_seed );
} );
BENCHMARK( bm3 , {
		generate_keypair( &pk , &sk , sk_seed );
} );

	}

	byte_fdump( stdout , "csk seed", csk.sk_seed , 16 ); puts("");
	byte_fdump( stdout , " sk seed", sk.sk_seed , 16 );  puts("");

	char msg[256];

	printf("benchmark:\n");

	printf("classic version.\n");
	bm_dump( msg , 256 , &bm2 );
	printf("gen sec key: %s.\n", msg );
	bm_dump( msg , 256 , &bm3 );
	printf("gen keypair: %s.\n", msg );

	printf("\n");
	printf("cyclic version.\n");
	bm_dump( msg , 256 , &bm0 );
	printf("gen sec key: %s.\n", msg );
	bm_dump( msg , 256 , &bm1 );
	printf("gen keypair: %s.\n", msg );


	return 0;
}

