
#include "ut_gf256.h"



#include "gf16.h"
/// where the real code in.




unsigned char ut_gf256_mul( unsigned char a , unsigned char b ) {
	return gf256_mul( a , b );
}

unsigned char ut_gf256_squ( unsigned char a ) {
	return gf256_squ( a );
}

unsigned char ut_gf256_inv( unsigned char a ) {
	return gf256_inv( a );
}

unsigned char ut_gf256_is_nonzero( unsigned char a ) {
	return gf256_is_nonzero( a );
}





unsigned char ut_gf256_mul_gf16( unsigned char a , unsigned char gf16_b ) {
	return gf256_mul_gf16( a , gf16_b );
}

