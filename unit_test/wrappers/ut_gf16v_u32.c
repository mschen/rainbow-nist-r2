
#include "ut_gf16v_u32.h"



#include "gf16.h"
/// where the real code in.



uint32_t ut_gf16v_mul_u32( uint32_t a , unsigned char b ) {
	return gf16v_mul_u32( a , b );
}


uint32_t ut_gf16v_squ_u32( uint32_t a ) {
	return gf16v_squ_u32( a );
}


uint32_t ut_gf16v_mul_8_u32( uint32_t a ) {
	return gf16v_mul_8_u32( a );
}


