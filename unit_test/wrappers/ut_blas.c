

#include "blas.h"


void ut_gf256v_add( uint8_t * accu_b, const uint8_t * a , unsigned _num_byte ) {
	gf256v_add( accu_b , a , _num_byte );
}


void ut_gf16v_mul_scalar( uint8_t * a, uint8_t b , unsigned _num_byte ) {
	gf16v_mul_scalar( a , b ,_num_byte );
}

void ut_gf256v_mul_scalar( uint8_t *a, uint8_t b, unsigned _num_byte ) {
	gf256v_mul_scalar( a , b , _num_byte );
}


void ut_gf16v_madd( uint8_t * accu_c, const uint8_t * a , uint8_t b, unsigned _num_byte ) {
	gf16v_madd( accu_c , a , b , _num_byte );
}

void ut_gf256v_madd( uint8_t * accu_c, const uint8_t * a , uint8_t b, unsigned _num_byte ) {
	gf256v_madd( accu_c , a , b , _num_byte );
}


