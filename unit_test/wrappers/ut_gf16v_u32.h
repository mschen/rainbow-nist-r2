#ifndef _UT_GF16V_U32_H_
#define _UT_GF16V_U32_H_


#include "stdint.h"


#ifdef  __cplusplus
extern  "C" {
#endif


uint32_t ut_gf16v_mul_u32( uint32_t a , unsigned char b );

uint32_t ut_gf16v_squ_u32( uint32_t a );

uint32_t ut_gf16v_mul_8_u32( uint32_t a );


#ifdef  __cplusplus
}
#endif

#endif
