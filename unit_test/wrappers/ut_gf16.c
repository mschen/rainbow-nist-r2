
#include "ut_gf16.h"



#include "gf16.h"
/// where the real code in.



unsigned char ut_gf16_is_nonzero( unsigned char a ) {
	return gf16_is_nonzero( a );
}

unsigned char ut_gf16_mul( unsigned char a , unsigned char b ) {
	return gf16_mul( a , b );
}

unsigned char ut_gf16_squ( unsigned char a ) {
	return gf16_squ(a);
}

unsigned char ut_gf16_inv( unsigned char a ) {
	return gf16_inv(a);
}

unsigned char ut_gf16_mul_4( unsigned char a ) {
	return gf16_mul_4( a );
}

unsigned char ut_gf16_mul_8( unsigned char a ) {
	return gf16_mul_8( a );
}

