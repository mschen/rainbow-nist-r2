
#include <stdio.h>

#include "rainbow_config.h"

#include "rainbow_keypair.h"

#include "rainbow.h"

#include "utils_prng.h"

#include "utils.h"

#define TEST_RUN 1000





#include "benchmark.h"

int main()
{
//	bm_init( &bmm );
	//unsigned char seed[32] = {0};

        printf("%s\n", _S_NAME );

        printf("sk size: %lu\n", sizeof(sk_t) );
        printf("pk size: %lu\n", sizeof(pk_t) );
        printf("csk size: %lu\n", sizeof(csk_t) );
        printf("cpk size: %lu\n", sizeof(cpk_t) );
        printf("digest size: %d\n", _PUB_M_BYTE );
        printf("signature size: %d\n\n", _PUB_N_BYTE );


	struct benchmark bm0,bm1,bm2,bm3;
	bm_init(&bm0);
	bm_init(&bm1);
	bm_init(&bm2);
	bm_init(&bm3);


	printf("\n\n============= setup PRNG ==============\n");

	prng_t _prng;
	prng_t * prng0 = &_prng;
	uint8_t prng_seed[32] = {0};
	prng_set( prng0 , prng_seed , 32 );

	printf("\n\n============= key pair generation ==============\n");

	pk_t _pk;
	sk_t _sk;
	pk_t * pk = &_pk;
	sk_t * sk = &_sk;

	uint8_t sk_seed[LEN_SKSEED] = {0};
	generate_keypair( pk , sk , sk_seed );

	printf("\n\n============= salt sign/verify test ==============\n");

	uint8_t  digest1[_HASH_LEN];
	prng_gen( prng0 , digest1,_HASH_LEN);
	byte_fdump( stdout , "dgst: " , digest1 , _HASH_LEN ); printf("\n");

	uint8_t signature1[_SIGNATURE_BYTE];
	rainbow_sign( signature1 , sk , digest1 );
	byte_fdump( stdout , "sig : " , signature1 , _SIGNATURE_BYTE ); printf("\n");

	int r = rainbow_verify( digest1 , signature1 , pk );
	printf("verify: %d (0 is success.)\n", r );

	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , digest1,_HASH_LEN);

BENCHMARK( bm0 , {
		rainbow_sign( signature1 , sk , digest1 );
} );
BENCHMARK( bm1 , {
		r = rainbow_verify( digest1 , signature1 , pk );
} );

		if( 0 != r ) {
			printf("fail:[%d]\n",i);
			byte_fdump( stdout , "dgst: " , digest1 , _HASH_LEN ); printf("\n");
			byte_fdump( stdout , "sig : " , signature1 , _SIGNATURE_BYTE ); printf("\n");
			printf("verify: %d (0 is success.)\n", r );
			return -1;
		}
	}
	printf("%d pk/sk test success.\n", TEST_RUN);


	printf("\n\n============= cyclic version ==============\n");

	cpk_t _cpk;
	csk_t _csk;
	cpk_t * cpk = &_cpk;
	csk_t * csk = &_csk;

	uint8_t pk_seed[LEN_PKSEED] = {0};
	generate_compact_keypair_cyclic( cpk , csk , pk_seed , sk_seed );

	printf("\n\n============= salt sign/verify test ==============\n");

	prng_gen( prng0 , digest1,_HASH_LEN);
	byte_fdump( stdout , "dgst: " , digest1 , _HASH_LEN ); printf("\n");

	rainbow_sign_cyclic( signature1 , csk , digest1 );
	byte_fdump( stdout , "sig : " , signature1 , _SIGNATURE_BYTE ); printf("\n");

	r = rainbow_verify_cyclic( digest1 , signature1 , cpk );
	printf("verify: %d (0 is success.)\n", r );

	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , digest1,_HASH_LEN);
BENCHMARK( bm2 , {
		rainbow_sign_cyclic( signature1 , csk , digest1 );
} );
BENCHMARK( bm3 , {
		r = rainbow_verify_cyclic( digest1 , signature1 , cpk );
} );
		if( 0 != r ) {
			printf("fail:[%d]\n",i);
			byte_fdump( stdout , "dgst: " , digest1 , _HASH_LEN ); printf("\n");
			byte_fdump( stdout , "sig : " , signature1 , _SIGNATURE_BYTE ); printf("\n");
			printf("verify: %d (0 is success.)\n", r );
			return -1;
		}
	}
	printf("%d pk/sk test success.\n", TEST_RUN);

	printf("\n\n============= benchmarker ==============\n");


        char msg[256];

	printf("benchmark:\n");

	printf("classic version.\n");
	bm_dump( msg , 256 , &bm0 );
	printf("  sign(): %s.\n", msg );
	bm_dump( msg , 256 , &bm1 );
	printf("verify(): %s.\n", msg );

	printf("\n");
	printf("cyclic version.\n");
	bm_dump( msg , 256 , &bm2 );
	printf("  sign(): %s.\n", msg );
	bm_dump( msg , 256 , &bm3 );
	printf("verify(): %s.\n", msg );

	return 0;
}

