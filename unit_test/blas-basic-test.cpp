
#include <stdio.h>


#include "ut_gf16.h"

#include "ut_gf256.h"

#include "ut_blas.h"





#define LEN_VECTOR 63


void initialize_vector( uint8_t * vec )
{
	for(unsigned i=0;i<LEN_VECTOR;i++) {
		vec[i] = (i*7)&0xff;
	}
}


int main()
{


	printf("\n\n");
	printf("  Tester for lib of basic linear algebra.\n\n");
	printf("  Presuming the correctness of ut_gf16_mul() and ut_gf256_mul().\n\n" );
	printf("  Default testing vector length: %d.\n", LEN_VECTOR );
	printf("  ut_gf256v_add()            :   vector XOR\n" );
	printf("  ut_gf16v_mul_scalar()      :   vector scalar multiplication in GF(16)\n" );
	printf("  ut_gf16v_madd()            :   vector multiplies a scalar and XOR to results in GF(16)\n" );
	printf("  ut_gf256v_mul_scalar()     :   vector scalar multiplication in GF(16)\n" );
	printf("  ut_gf256v_madd()           :    vector multiplies a scalar and XOR to results in GF(16)\n" );


	uint8_t a[LEN_VECTOR];
	uint8_t b[LEN_VECTOR];
	uint8_t c[LEN_VECTOR];

	printf("\n\n============= Tester for ut_gf256v_add() function ==============\n");

	initialize_vector( a );
	initialize_vector( b );
	initialize_vector( c );

	ut_gf256v_add( b , a , LEN_VECTOR );
	for(unsigned i=0;i<LEN_VECTOR;i++) {
		if( 0 != b[i] ) {
			printf("test:v[%d]^v[%d]:\n",i,i);
			printf("expect: 0\n" );
			printf("got (%d) : %x\n", i , b[i] );
			goto L_FAIL;
		}
	}

	printf("\nPASS.\n");

	printf("\n\n============= Tester for ut_gf16v_mul_scalar() function ==============\n");

	initialize_vector( a );
	initialize_vector( b );
	initialize_vector( c );

	for(unsigned i=0;i<16;i++) {
		initialize_vector( a );
		ut_gf16v_mul_scalar( a , i , LEN_VECTOR );

		for(unsigned j=0;j<LEN_VECTOR;j++) {
			uint8_t d = ut_gf256_mul( b[j] , i );
			if( d != a[j] ) {
				printf("test:v[%d]x%x:\n",j,i);
				printf("expect: %x\n", d );
				printf("got   : %x\n", a[j] );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n\n");


	printf("\n\n============= Tester for ut_gf16v_muadd() function ==============\n");

	initialize_vector( a );
	initialize_vector( b );
	initialize_vector( c );

	for(unsigned i=0;i<16;i++) {
		initialize_vector( a );
		ut_gf16v_madd( a , b , i , LEN_VECTOR );

		for(unsigned j=0;j<LEN_VECTOR;j++) {
			uint8_t d = c[j] ^ ut_gf256_mul( b[j] , i );
			if( d != a[j] ) {
				printf("test:v[%d] madd %x:\n",j,i);
				printf("expect: %x\n", d );
				printf("got   : %x\n", a[j] );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n\n");



	printf("\n\n============= Tester for ut_gf256v_mul_scalar() function ==============\n");

	initialize_vector( a );
	initialize_vector( b );
	initialize_vector( c );

	for(unsigned i=0;i<256;i++) {
		initialize_vector( a );
		ut_gf256v_mul_scalar( a , i , LEN_VECTOR );

		for(unsigned j=0;j<LEN_VECTOR;j++) {
			uint8_t d = ut_gf256_mul( b[j] , i );
			if( d != a[j] ) {
				printf("test:v[%d]x%x:\n",j,i);
				printf("expect: %x\n", d );
				printf("got   : %x\n", a[j] );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n\n");


	printf("\n\n============= Tester for ut_gf256v_muadd() function ==============\n");

	initialize_vector( a );
	initialize_vector( b );
	initialize_vector( c );

	for(unsigned i=0;i<256;i++) {
		initialize_vector( a );
		ut_gf256v_madd( a , b , i , LEN_VECTOR );

		for(unsigned j=0;j<LEN_VECTOR;j++) {
			uint8_t d = c[j] ^ ut_gf256_mul( b[j] , i );
			if( d != a[j] ) {
				printf("test:v[%d] madd %x:\n",j,i);
				printf("expect: %x\n", d );
				printf("got   : %x\n", a[j] );
				goto L_FAIL;
			}
		}
	}

	printf("\nPASS.\n\n");




	return 0;

L_FAIL:
	printf("\nFIAL.\n");
	return -1;
}

