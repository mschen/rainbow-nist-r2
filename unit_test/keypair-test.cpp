
#include <stdio.h>

#include "rainbow_keypair.h"

#include "blas.h"
#include "utils.h"

#define TEST_RUN 100




#include "benchmark.h"



const unsigned char * set_hexstring( unsigned char * res , const char * str , unsigned len )
{
  char byte[4] = {0};
  while( len-- ) {
    byte[0] = str[0];
    byte[1] = str[1];
    unsigned rr;
    sscanf( byte , "%x" , &rr );
    res[0] = (unsigned char)(rr&0xff);
    res ++;
    str += 2;
  }
  return res;
}



const char * tc0_l2_F1 = "06896b9f17a70346998ec371a8dcfafb4df942ba419986b7fb1ab69c3c208044";
const char * tc0_l2_F1s[33] = {
"06896b9f17a70346998ec371a8dcfafb",
"4df942ba419986b7fb1ab69c3c208044",
"14ff408bd100a36edce28a87deeb1ecd",
"bf4de7587b527367db03493987471586",
"ef056f5d0ad8820df97349b5e5fcb6f7",
"a424f1fca9fd6073228e9ba647af307e",
"2d402b437cf54d4ef4b6eeab8a23d52c",
"bbe7fbdc14246a7c4bd157143115769c",
"5f36632cb036610987d1780ad9e70a47",
"76e2b80216cadfd0f30cf97142ac2f5d",
"d1eeecef2566edfb506a1eff1928523a",
"8f285c05c90741bddc8f4d23027f3687",
"f795508521abb7e99f9bca9bb04125f8",
"7aa79a269bf251bd0f9b58a6745d9145",
"5813a9897a8019bf970448fad182f4cd",
"64658e0d0162d09a64235262d326ab45",
"1a99ae907ffe4bb3d6f2fd14e2f7d383",
"5d4b72cf866635b6f45c18d8c51a43f7",
"886fc19ff8af9ae0b6ec5d053a7132b6",
"06a50af853174b1defa4ed987a79b67c",
"fee66a44ae493e461f978b6b976c452c",
"65c1edef26e368bc063583fbff441a72",
"5411a15d883998494162cdba5e48a0bd",
"82348da3bf53c4dc261f5a570c272d1d",
"5ed221da2b655289d5c337630a954886",
"39331e57327fdde18f95e280fb92a6c1",
"cb1643f48844a11152e28c38244a126d",
"372566f0ae135c1f9c072b3c1f96acd8",
"19648a3457b6af78730e5acc00127f61",
"97065486a32d4c266399841ebfc0ad55",
"8eecbbeafb5f9a29f3900468fce351f5",
"9173fd4d05d42877dc2615d2cfd09460",
"c0ed6e2545068181994b37a3f61ec0ae" };




const char * tc0_l2_F2 = "8496b1360f211209a351945abbacf405064d6cf520e735242b9ad70a1ecf323e";
const char * tc0_l2_F2s[33] = {
"8496b1360f211209a351945abbacf405",
"064d6cf520e735242b9ad70a1ecf323e",
"44ad36b68912594bc80aaaf29718cdb2",
"ba65ca4a9ce7e5682bf112d9573765ba",
"877188aeaa17307c15ff3988e4fe01e2",
"4af03609d506e1663cfd23c45272d581",
"8cf8efc8affc4e8255f15a72004899be",
"b31dd45f91cab635bcd54661020c6b16",
"75b43ed51079a3849a6f68d5bf4577f1",
"7dccc32b1947a08fe1995e37943f7570",
"e8b0cceed33781a3540ef9bfd75edc71",
"3d4ed9838c3fb0afdce3daaf00e57042",
"e31a0e5d783da5767800c48aa8703bcb",
"0d7023e078f4b575144aa20b86593b61",
"381fcc4c1ecaa39332bbd454850a8e38",
"5014edd788c25e029444fdad87ab3b9a",
"fedc3fec9ecd7a94e4913ee89141d68f",
"9552419c599802833649b56e80e1d1e7",
"e8f1d69ec4af18849860aa9ade0fb749",
"e76d88d6fe9cf2ac149a218e863327f4",
"1017a0b8ce1e580c80e797bdea493ce1",
"62bec9aa5fa79994a76d294e139b519d",
"7de0426bea59ad88fe5b0e69a56827fd",
"d03bf33e75098e1fb163340b38045346",
"84e5557edb8573bf6a4255ad27d15960",
"c2718058992d6a038dc99d879c6b58f5",
"240108fb23bbcb78b5f7148390ff8ea9",
"37ab773a4913a0da1e0b104b3e08160c",
"9d2e9236e09efbfee5ea0471409ffccd",
"4089b49120598b340e78e65e2bc1b90c",
"bbccb3fe02dc462a07fb5fb4a9ee32e7",
"19f1819b2349512e5094e75b2f3e63c1",
"6d2741ad8da00a45e30ea16bc828f250" };



const char * tc0_l2_F3 = "fe575125444517654426b36973d63ebc2c160fde66a03bc433fdb3cf722ebd59";
const char * tc0_l2_F3s[33] = {
"fe575125444517654426b36973d63ebc",
"2c160fde66a03bc433fdb3cf722ebd59",
"5a279c09a60383db2c656eb804ca1a22",
"9bab2b0573439c4a720c4c9138a6546f",
"dacb948768a0376fdc8a05ab29c409ca",
"50ef0d2f4d2e53902dece71a68017c11",
"d26806d4b4acd61cfa2f86f62e584a45",
"e04f2655c9336f2fbd6f7ff9e0acb815",
"8565482730ebbbde990735d39c7823de",
"2756dde9531b324da45b0f4cd0770c74",
"7dd1a5bdf2def8798d2da004e0636302",
"d0b4802169986d4b5a5a28ef56baad5b",
"bb97eae789b711bc21271e1c9faceec6",
"c2d77411f800b7022709fe28585c17bd",
"ab28d3b912d4b4165078ac630bca1a0f",
"f5928d037afc02a0bde0ad7b12cb3b42",
"5f14199905a0e92db1853a335a354be0",
"41cd3ec5c7f767f0ea2962e0586c9522",
"f94db0a0d553290b146905d746bd24f7",
"68363cac352cb8b6536ead0fc650cfb3",
"728352fadd89f48c7a74c326576e36aa",
"4f74063af90f0812e164fb013cb5a6fa",
"8ae131d425f32dea9e977fecbc11768e",
"feb864af074716b3bfeb82aa75e8c37a",
"ab8809fdcf19f12c64576a719df66686",
"954979c58f51fb04cf9eaa226df4f593",
"1ffdd66302faa6d9e6af36a614e97a45",
"74eb1880d54f147e212c0315ec8461e6",
"a23713c2be3b952c1a704f5a351a914f",
"98051e81485681edc8de3dd68aad362f",
"3e62031594fc85e007aa88d3d3f04a2e",
"cc810069fa51f5ceb8af669806b60b68",
"b9089e7bdedbad1756ef01827642c51a" };


const char * tc0_l2_F5 = "21e7053556eec44807e2e040a8a87a21160790ae2b61566e354b93321df1a743";
const char * tc0_l2_F5_0 = "21e7053556eec44807e2e040a8a87a21";
const char * tc0_l2_F5_1 = "160790ae2b61566e354b93321df1a743";

const char * tc0_l2_F6 = "6d27bc3cbdb5d42d95b9bd110d0493a2ec45d5bf544f4e412550c5de573b1e34";

const char * tc0_l1_F1_0 = "1986712156058e0d5439eba92207e04f";

const char * tc0_l1_F2 = "5e2f3491fc3baed43f5f968f812e0524950cc33afcda31edb1468123f9cd077c";
const char * tc0_l1_F2s[33] = {
"5e2f3491fc3baed43f5f968f812e0524",
"950cc33afcda31edb1468123f9cd077c",
"34ee78bf3b94e3316e3c69548e539436",
"fab8b263a6b82072ca62818674c7a5b2",
"c6732c5a67d62e16332968d5ef385039",
"4f99b3e9de73f3b1b1e3cd4ebf97867c",
"0b4b5c2da25fbf51b043730eca40e652",
"b15a13beb491ed191323767ec52294ae",
"ba0ca28865fe182f58e631c692524b80",
"d76f6877b5df7537dfeb127374ce97e1",
"dc26aeaa6dd06eb848923121c9ff12e4",
"4e07763add175a072dd7c56c47f12d1f",
"583bf4aac489eff1cb97aacada987801",
"863ef0e10b2c30a8f19878203dd577d0",
"0d6692bcbaf0cd60c6a357f558fa0e1a",
"787c3d71b51e344e8b4dad85a8e502dd",
"0285e7f6ffbe7bbe61b3442b5cf07f75",
"de97b48d0f46f1d134d42bb0d89b5497",
"addf096bfd82519d7a14dd7c8c1b5ab7",
"2dd435946f142417eb3cba6be40e87e6",
"2b219a290a0574aeca16236f7d7c3723",
"d25f6704ae250836727f60bf4998a599",
"e794b1bcda893ab9890b6bb5a0b81ab2",
"9dfecac853820e24262b9ff594511607",
"27bd38176292f7c0f59c85feda1939e0",
"7041756331bdd37d6bc2c0812614b63a",
"6f80bc4e93b815fa8f58faa0fd5927a9",
"393048e35b4358de9b03074120f9ce8a",
"399d6f11d4541e058f55f8680dd11125",
"8de71c12860d4456a3f6019d69764903",
"e1272cb03da97a82eb4a7b708d7a394d",
"b42790f47b4398b50904d25999c68cce",
"54b78accef2269742ff6cd98643586f6" };


const char * tc0_s1 =      "66687aadf862bd776c8fc18b8e9f8e20";
const char * tc0_t1 =      "43198db7fe2baee6f10c3434d1a42ac6";


int test_0( const csk_t * _sk )
{
  sk_t inst_sk;
  sk_t * sk = &inst_sk;
  generate_secretkey_cyclic( sk , _sk->pk_seed , _sk->sk_seed );
  puts("test 0: secret key from sk seed = {0}");
  byte_fdump( stdout , "sk_seed: " , sk->sk_seed , LEN_SKSEED );
  puts("");


  unsigned char buf[32];

  byte_fdump( stdout , "sk->s1: " , sk->s1 , 16 );
  set_hexstring( buf , tc0_s1 , 16 );
  gf256v_add( buf , sk->s1 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_s1 );
    return -1;
  } else {
    puts(" : PASSED.");
  }

  byte_fdump( stdout , "sk->t1: " , sk->t1 , 16 );
  set_hexstring( buf , tc0_t1 , 16 );
  gf256v_add( buf , sk->t1 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_t1 );
    return -1;
  } else {
    puts(" : PASSED.");
  }

  byte_fdump( stdout , "sk->l1F2: " , sk->l1_F2 , 32 );
  set_hexstring( buf , tc0_l1_F2 , 32 );
  gf256v_add( buf , sk->l1_F2 , 32 );
  if( !gf256v_is_zero(buf,32) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l1_F2 );
    return -1;
  } else {
    puts(" : PASSED.");
  }

  for(int i=0;i<33;i++) {
    printf("check sk->l1F2s[%d]: ", i );
    byte_fdump( stdout , "" , sk->l1_F2 + 16*i , 16 );
    set_hexstring( buf , tc0_l1_F2s[i] , 16 );
    gf256v_add( buf , sk->l1_F2 + 16*i , 16 );
    if( !gf256v_is_zero(buf,16) ) {
      printf(" : FAILED.\ncorr  :  = %s\n", tc0_l1_F2s[i] );
      return -1;
    } else {
      puts(" : PASSED.");
    }
  }



  byte_fdump( stdout , "sk->l2F1: " , sk->l2_F1 , 32 );
  set_hexstring( buf , tc0_l2_F1 , 32 );
  gf256v_add( buf , sk->l2_F1 , 32 );
  if( !gf256v_is_zero(buf,32) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F1 );
    return -1;
  } else {
    puts(" : PASSED.");
  }

  for(int i=0;i<33;i++) {
    printf("check sk->l2F1s[%d]: ", i );
    byte_fdump( stdout , "" , sk->l2_F1 + 16*i , 16 );
    set_hexstring( buf , tc0_l2_F1s[i] , 16 );
    gf256v_add( buf , sk->l2_F1 + 16*i , 16 );
    if( !gf256v_is_zero(buf,16) ) {
      printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F1s[i] );
      return -1;
    } else {
      puts(" : PASSED.");
    }
  }


  byte_fdump( stdout , "sk->l2F2: " , sk->l2_F2 , 32 );
  set_hexstring( buf , tc0_l2_F2 , 32 );
  gf256v_add( buf , sk->l2_F2 , 32 );
  if( !gf256v_is_zero(buf,32) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F2 );
    return -1;
  } else {
    puts(" : PASSED.");
  }

  for(int i=0;i<33;i++) {
    printf("check sk->l2F2s[%d]: ", i );
    byte_fdump( stdout , "" , sk->l2_F2 + 16*i , 16 );
    set_hexstring( buf , tc0_l2_F2s[i] , 16 );
    gf256v_add( buf , sk->l2_F2 + 16*i , 16 );
    if( !gf256v_is_zero(buf,16) ) {
      printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F2s[i] );
      return -1;
    } else {
      puts(" : PASSED.");
    }
  }

  byte_fdump( stdout , "sk->l2F3: " , sk->l2_F3 , 32 );
  set_hexstring( buf , tc0_l2_F3 , 32 );
  gf256v_add( buf , sk->l2_F3 , 32 );
  if( !gf256v_is_zero(buf,32) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F3 );
    return -1;
  } else {
    puts(" : PASSED.");
  }

  for(int i=0;i<33;i++) {
    printf("check sk->l2F3s[%d]: ", i );
    byte_fdump( stdout , "" , sk->l2_F3 + 16*i , 16 );
    set_hexstring( buf , tc0_l2_F3s[i] , 16 );
    gf256v_add( buf , sk->l2_F3 + 16*i , 16 );
    if( !gf256v_is_zero(buf,16) ) {
      printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F3s[i] );
      return -1;
    } else {
      puts(" : PASSED.");
    }
  }


  byte_fdump( stdout , "sk->l2F5: " , sk->l2_F5 , 32 );
  set_hexstring( buf , tc0_l2_F5 , 32 );
  gf256v_add( buf , sk->l2_F5 , 32 );
  if( !gf256v_is_zero(buf,32) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F5 );
    return -1;
  } else {
    puts(" : PASSED.");
  }


  byte_fdump( stdout , "sk->l2F6: " , sk->l2_F6 , 32 );
  set_hexstring( buf , tc0_l2_F6 , 32 );
  gf256v_add( buf , sk->l2_F6 , 32 );
  if( !gf256v_is_zero(buf,32) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l2_F6 );
    return -1;
  } else {
    puts(" : PASSED.");
  }


/*
  byte_fdump( stdout , "sk->F2: " , (sk->l1_F2)+16 , 16 );
  set_hexstring( buf , tc0_l1_F2_1 , 16 );
  gf256v_add( buf , (sk->l1_F2)+16 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc0_l1_F2_1 );
    return -1;
  } else {
    puts(" : PASSED.");
  }
*/

  return 0;

}



const char * tc1_l1_Q3    = "4c0417f1a0f3cc8a8b7cb091fad76108";
const char * tc1_l1_Q3_32 = "9eaefdebf15a106ce6e301a9cfcb1483";

const char * tc1_l1_Q5 = "db552e2984be21d029bf3ca596dc5028";
const char * tc1_l1_Q5_32 = "d8dffbb26946ded57c9ff7be367f0ab4";

const char * tc1_l1_Q6 = "495158fd57e5ba7c45682a7918f961af";
const char * tc1_l1_Q6_32 = "82eae0a02ddf17111840bf5c633d606a";

const char * tc1_l1_Q9 = "2ed90f150f391bc6daf6aa824a7f93e3";
const char * tc1_l1_Q9_32 = "fcbdfdce0e05288c63d932e8943dc098";

const char * tc1_l2_Q9 = "f7d510a690f9ddd26d2e117a7d9edf60";
const char * tc1_l2_Q9_32 = "8a0dc6de76a99ad424599ee41134bbfc";



int test_1( const cpk_t * pk )
{
  puts("test 0: public key from pk/sk seed = {0}");
  byte_fdump( stdout , "pk_seed: " , pk->pk_seed , LEN_PKSEED );
  puts("");

  unsigned char buf[32];


  byte_fdump( stdout , "pk->l1Q3: " , pk->l1_Q3 , 16 );
  set_hexstring( buf , tc1_l1_Q3 , 16 );
  gf256v_add( buf , pk->l1_Q3 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q3 );
    return -1;
  } else {
    puts(" : PASSED.");
  }
  byte_fdump( stdout , "pk->l1Q3: " , pk->l1_Q3 + 16*32 , 16 );
  set_hexstring( buf , tc1_l1_Q3_32 , 16 );
  gf256v_add( buf , pk->l1_Q3 + 16*32 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q3_32 );
    return -1;
  } else {
    puts(" : PASSED.");
  }



  byte_fdump( stdout , "pk->l1Q5: " , pk->l1_Q5 , 16 );
  set_hexstring( buf , tc1_l1_Q5 , 16 );
  gf256v_add( buf , pk->l1_Q5 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q5 );
    return -1;
  } else {
    puts(" : PASSED.");
  }
  byte_fdump( stdout , "pk->l1Q5: " , pk->l1_Q5 + 16*32 , 16 );
  set_hexstring( buf , tc1_l1_Q5_32 , 16 );
  gf256v_add( buf , pk->l1_Q5 + 16*32 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q5_32 );
    return -1;
  } else {
    puts(" : PASSED.");
  }


  byte_fdump( stdout , "pk->l1Q6: " , pk->l1_Q6 , 16 );
  set_hexstring( buf , tc1_l1_Q6 , 16 );
  gf256v_add( buf , pk->l1_Q6 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q6 );
    return -1;
  } else {
    puts(" : PASSED.");
  }
  byte_fdump( stdout , "pk->l1Q6: " , pk->l1_Q6 + 16*32 , 16 );
  set_hexstring( buf , tc1_l1_Q6_32 , 16 );
  gf256v_add( buf , pk->l1_Q6 + 16*32 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q6_32 );
    return -1;
  } else {
    puts(" : PASSED.");
  }


  byte_fdump( stdout , "pk->l1Q9: " , pk->l1_Q9 , 16 );
  set_hexstring( buf , tc1_l1_Q9 , 16 );
  gf256v_add( buf , pk->l1_Q9 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q9 );
    return -1;
  } else {
    puts(" : PASSED.");
  }
  byte_fdump( stdout , "pk->l1Q9: " , pk->l1_Q9 + 16*32 , 16 );
  set_hexstring( buf , tc1_l1_Q9_32 , 16 );
  gf256v_add( buf , pk->l1_Q9 + 16*32 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l1_Q9_32 );
    return -1;
  } else {
    puts(" : PASSED.");
  }


  byte_fdump( stdout , "pk->l2Q9: " , pk->l2_Q9 , 16 );
  set_hexstring( buf , tc1_l2_Q9 , 16 );
  gf256v_add( buf , pk->l2_Q9 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l2_Q9 );
    return -1;
  } else {
    puts(" : PASSED.");
  }
  byte_fdump( stdout , "pk->l2Q9: " , pk->l2_Q9 + 16*32 , 16 );
  set_hexstring( buf , tc1_l2_Q9_32 , 16 );
  gf256v_add( buf , pk->l2_Q9 + 16*32 , 16 );
  if( !gf256v_is_zero(buf,16) ) {
    printf(" : FAILED.\ncorr  :  = %s\n", tc1_l2_Q9_32 );
    return -1;
  } else {
    puts(" : PASSED.");
  }


  return 0;

}




int main()
{

        printf("%s\n", _S_NAME );

        printf("sk size: %lu\n", sizeof(sk_t) );
        printf("pk size: %lu\n", sizeof(pk_t) );
        printf("csk size: %lu\n", sizeof(csk_t) );
        printf("cpk size: %lu\n", sizeof(cpk_t) );
        printf("digest size: %d\n", _PUB_M_BYTE );
        printf("signature size: %d\n\n", _PUB_N_BYTE );


	unsigned char pk_seed[LEN_PKSEED] = {0};
	unsigned char sk_seed[LEN_SKSEED] = {0};

	int err = 0;

	csk_t sk;
	cpk_t pk;

	printf("comparing equality of generated keys with sage results.\n");
	printf("can only test for rainbow(16,32,32,32) and debugged PRNG(hash chains). \n");

	printf("\n\n============= generating keys ==============\n");

	generate_compact_keypair_cyclic( &pk , &sk , pk_seed , sk_seed );

	err = test_0( &sk );
	if( err ) goto L_ERR;

	err = test_1( &pk );
	if( err ) goto L_ERR;





	printf("pk/sk test success.\n");
	return 0;
L_ERR:
	printf("err: %d.\n", err );
	return -1;
}

