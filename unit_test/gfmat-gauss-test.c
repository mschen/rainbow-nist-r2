
#include <stdio.h>

#include "rainbow_config.h"

#include "rainbow_keypair.h"

#include "rainbow.h"

#include "utils_prng.h"

#include "utils.h"

#include "blas_comm.h"

#include "blas_matrix.h"

#include "blas.h"

#define TEST_RUN 1000


#include "benchmark.h"



int main()
{

	printf("\n===============================================================\n\n");
	printf("  Tester for Gauss elimination.\n\n");
	printf("\n===============================================================\n\n");

        char msg[256];
	struct benchmark bm0,bm1;

	unsigned char mat0[64*64];
	unsigned char mat1[64*64];
	//unsigned char mat2[64*64];

	unsigned char y[64];
	unsigned char x[64];
	unsigned char b[64];

	// setup prng
	prng_t _prng;
	prng_t *prng0 = &_prng;
	for(unsigned i=0;i<sizeof(y);i++) y[i] = i;
	prng_set( prng0 , y , sizeof(y) );


	printf("\n===============================================================\n\n");
	printf(" Test for inverting gf16mat 32x32\n\n");

	int num_succ = 0;
	bm_init(&bm0);
	bm_init(&bm1);
	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , mat0 , 32*16 );
		prng_gen( prng0 , b , 16 );

		int r0,r1;
BENCHMARK( bm0 , {
		r0 = gf16mat_inv_32x32( mat1 , mat0 );
} );
BENCHMARK( bm1 , {
		r1 = gf16mat_solve_linear_eq_32x32( x, mat0, b );
} );
#if 1
		if( r0 != r1 ) {
			printf("fail:[%d]\n",i);
			printf("inconsistent: r0,r1 = %d,%d\n", r0 , r1 );
			return -1;
		}
#endif
		if( r0 ) {
			num_succ ++;
			gf16mat_prod( y , mat0 , 16 , 32 , x );

			gf256v_add( y , b , 16 );
			unsigned eq = gf256v_is_zero( y , 16 );

#if 1
			if( !eq ) {
				printf("fail:[%d]\n",i);
				printf("is gf16mat_inv() and gf16mat_solve_linear_eq equal ? %d\n", eq );
				return -1;
			}
#endif
		}


	}
	printf("%d/%d invertable matrix\n", num_succ , TEST_RUN);
	printf("gf16mat_inv():\n");
	bm_dump( msg , sizeof(msg) , &bm0 );
	printf("%s\n", msg );
	printf("gf16mat_solve_linear_eq():\n" );
	bm_dump( msg , sizeof(msg) , &bm1 );
	printf("%s\n", msg );



////////////////////////////////////////////////////////////////////

	printf("\n===============================================================\n\n");
	printf(" Test for inverting gf256mat 32x32\n\n");

	bm_init(&bm0);
	bm_init(&bm1);
	num_succ = 0;
	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , mat0 , 32*32 );
		prng_gen( prng0 , b , 32 );

		int r0;
BENCHMARK( bm0 , {
		r0 = gf256mat_inv_32x32( mat1 , mat0 );
} );
		if( r0 ) {
			num_succ ++;
			gf256mat_prod( x , mat0 , 32 , 32 , b );
			gf256mat_prod( y , mat1 , 32 , 32 , x );

			gf256v_add( y , b , 32 );
			unsigned eq = gf256v_is_zero( y , 32 );

			if( !eq ) {
				printf("fail:[%d]\n",i);
				return -1;
			}
		}


	}
	printf("%d/%d invertable matrix\n", num_succ , TEST_RUN);
	printf("gf256mat_inv():\n");
	bm_dump( msg , sizeof(msg) , &bm0 );
	printf("%s\n", msg );


////////////////////////////////////////////////////////////////////

	printf("\n===============================================================\n\n");
	printf(" Test for inverting gf256mat 36x36\n\n");

	bm_init(&bm0);
	bm_init(&bm1);
	num_succ = 0;
	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , mat0 , 36*36 );
		prng_gen( prng0 , b , 36 );

		int r0;
BENCHMARK( bm0 , {
		r0 = gf256mat_inv_36x36( mat1 , mat0 );
} );
		if( r0 ) {
			num_succ ++;
			gf256mat_prod( x , mat0 , 36 , 36 , b );
			gf256mat_prod( y , mat1 , 36 , 36 , x );

			gf256v_add( y , b , 36 );
			unsigned eq = gf256v_is_zero( y , 36 );

			if( !eq ) {
				printf("fail:[%d]\n",i);
				return -1;
			}
		}
	}
	printf("%d/%d invertable matrix\n", num_succ , TEST_RUN);
	printf("gf256mat_inv():\n");
	bm_dump( msg , sizeof(msg) , &bm0 );
	printf("%s\n", msg );


////////////////////////////////////////////////////////////////////

	printf("\n===============================================================\n\n");
	printf(" Test for solving linear equation 48x48\n\n");

	bm_init(&bm0);
	bm_init(&bm1);
	num_succ = 0;
	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , mat0 , 48*48 );
		prng_gen( prng0 , b , 48 );

		int r0;
BENCHMARK( bm0 , {
		r0 = gf256mat_solve_linear_eq_48x48( x, mat0, b );
} );
		if( r0 ) {
			num_succ ++;
			gf256mat_prod( y , mat0 , 48 , 48 , x );

			gf256v_add( y , b , 48 );
			unsigned eq = gf256v_is_zero( y , 48 );

			if( !eq ) {
				printf("fail:[%d]\n",i);
				return -1;
			}
		}
	}
	printf("%d/%d invertable matrix\n", num_succ , TEST_RUN);
	printf("gf256mat_solve_linear_eq():\n");
	bm_dump( msg , sizeof(msg) , &bm0 );
	printf("%s\n", msg );


////////////////////////////////////////////////////////////////////

	printf("\n===============================================================\n\n");
	printf(" Test for solving linear equation 64x64\n\n");

	bm_init(&bm0);
	bm_init(&bm1);
	num_succ = 0;
	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , mat0 , 64*64 );
		prng_gen( prng0 , b , 64 );

		int r0;
BENCHMARK( bm0 , {
		r0 = gf256mat_solve_linear_eq_64x64( x, mat0, b );
} );
		if( r0 ) {
			num_succ ++;
			gf256mat_prod( y , mat0 , 64 , 64 , x );

			gf256v_add( y , b , 64 );
			unsigned eq = gf256v_is_zero( y , 64 );

			if( !eq ) {
				printf("fail:[%d]\n",i);
				return -1;
			}
		}
	}
	printf("%d/%d invertable matrix\n", num_succ , TEST_RUN);
	printf("gf256mat_solve_linear_eq():\n");
	bm_dump( msg , sizeof(msg) , &bm0 );
	printf("%s\n", msg );


	return 0;
}

