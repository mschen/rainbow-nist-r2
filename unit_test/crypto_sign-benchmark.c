
#include <stdio.h>

#include "rainbow_config.h"

#include "rainbow_keypair.h"

#include "rainbow.h"

#include "utils_prng.h"

#include "utils.h"

#include "api.h"

#define TEST_RUN 500





#include "benchmark.h"

int main()
{
//	bm_init( &bmm );
	//unsigned char seed[32] = {0};

        printf("%s\n", CRYPTO_ALGNAME );

        printf("sk size: %lu\n", CRYPTO_SECRETKEYBYTES );
        printf("pk size: %lu\n", CRYPTO_PUBLICKEYBYTES );
        printf("digest size: %d\n", _PUB_M_BYTE );
        printf("signature size: %d\n\n", CRYPTO_BYTES );


	struct benchmark bm0,bm1,bm2;
	bm_init(&bm0);
	bm_init(&bm1);
	bm_init(&bm2);

	unsigned char pk[CRYPTO_PUBLICKEYBYTES];
	unsigned char sk[CRYPTO_SECRETKEYBYTES];

	unsigned char m[64];
	unsigned char m2[64];
	unsigned long long mlen;
	unsigned char sm[sizeof(m)+CRYPTO_BYTES];
	unsigned long long smlen;

	// setup prng
	prng_t _prng;
	prng_t *prng0 = &_prng;
	for(int i=0;i<sizeof(m);i++) m[i] = i;
	prng_set( prng0 , m , sizeof(m) );

	for(unsigned i=0;i<TEST_RUN;i++) {
		prng_gen( prng0 , m , sizeof(m) );

		int r0,r1,r2;
BENCHMARK( bm0 , {
		r0 = crypto_sign_keypair( pk , sk );
} );
BENCHMARK( bm1 , {
		r1 = crypto_sign( sm , &smlen , m , sizeof(m) , sk );
} );
BENCHMARK( bm2 , {
		r2 = crypto_sign_open( m2 , &mlen , sm , smlen , pk );
} );

		if( 0 != r0 || 0 != r1 || 0 != r2 ) {
			printf("fail:[%d]\n",i);
			printf("[keypair/sign/varify]: [%d/%d/%d]\n", r0 , r1, r2 );
			//byte_fdump( stdout , "dgst: " , digest1 , _HASH_LEN ); printf("\n");
			//byte_fdump( stdout , "sig : " , signature1 , _SIGNATURE_BYTE ); printf("\n");
			//printf("verify: %d (0 is success.)\n", r );
			return -1;
		}
	}
	printf("%d test success.\n", TEST_RUN);

	printf("\n\n============= benchmarker ==============\n");

        char msg[256];

	printf("benchmark:\n");

	printf("crypto_sign_keypair():\n");
	bm_dump( msg , sizeof(msg) , &bm0 );
	printf("%s\n", msg );

	printf("crypto_sign():\n" );
	bm_dump( msg , sizeof(msg) , &bm1 );
	printf("%s\n", msg );

	printf("crypto_sign_open():\n" );
	bm_dump( msg , sizeof(msg) , &bm2 );
	printf("%s\n", msg );


	printf("\n\n");

	return 0;
}

