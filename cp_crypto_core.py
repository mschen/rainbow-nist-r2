#!/usr/bin/python

import sys
import os, errno, shutil
from shutil import copyfile



src_dirs =  [ 'crypto_sign/rainbow1acompres363232' , 'crypto_sign/rainbow3ccompres683248' , 'crypto_sign/rainbow5ccompres963664' ]
dest_dirs = [ 'crypto_core/rainbowcalsecret363232' , 'crypto_core/rainbowcalsecret683248' , 'crypto_core/rainbowcalsecret963664' ]


for n in dest_dirs :
  try:
    print( 'create dir: ' + n )
    os.makedirs(n)
  except OSError as e:
    if e.errno != errno.EEXIST:
      raise


impls = [ 'ref' , 'amd64' , 'ssse3' , 'avx2' ]
for src in src_dirs :
  for imp in impls :
    s1 = src + '/' + imp
    d1 = dest_dirs[ src_dirs.index(src) ] + '/' + imp
    shutil.rmtree( d1 , ignore_errors=True )
    print( "copying: " + s1 + ' -> ' + d1  )
    shutil.copytree( s1 , d1 )


white_list = [ \
'api.h',\
'architectures',\
'goal-constbranch',\
'goal-constindex',\
'blas_avx2.h',\
'blas_comm.c',\
'blas_comm.h',\
'blas_config.h',\
'blas.h',\
'blas_matrix.h',\
'blas_matrix_ref.h',\
'blas_matrix_sse.h',\
'blas_matrix_avx2.h',\
'blas_sse.h',\
'blas_u32.h',\
'blas_u64.h',\
'gf16_avx2.h',\
'gf16.c',\
'gf16.h',\
'gf16_sse.h',\
'gf16_tabs.h',\
'gf16_u64.h',\
'parallel_matrix_op_avx2.c',\
'parallel_matrix_op_avx2.h',\
'parallel_matrix_op_sse.c',\
'parallel_matrix_op_sse.h',\
'parallel_matrix_op.c',\
'parallel_matrix_op.h',\
'rainbow_blas.h',\
'rainbow_blas_simd.h',\
'rainbow_config.h',\
'rainbow_keypair_computation.c',\
'rainbow_keypair_computation.h',\
'rainbow_keypair_computation_simd.c',\
'rainbow_keypair_computation_simd.h',\
'rainbow_keypair.h',\
'rng.h',\
'utils_malloc.h' ]


replace_dic = {
'rainbow_keypair_computation.c':
   ['//IF_CRYPTO_CORE:#include \"crypto_core.h\"' ,
'#include \"crypto_core.h\"' ] ,
'gf16_tabs.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define __mask_0x55 CRYPTO_NAMESPACE(__mask_0x55)
#define __mask_low CRYPTO_NAMESPACE(__mask_low)
#define __mask_16 CRYPTO_NAMESPACE(__mask_16)
#define __gf16_inv CRYPTO_NAMESPACE(__gf16_inv)
#define __gf16_squ CRYPTO_NAMESPACE(__gf16_squ)
#define __gf16_squ_x8 CRYPTO_NAMESPACE(__gf16_squ_x8)
#define __gf16_squ_sl4 CRYPTO_NAMESPACE(__gf16_squ_sl4)
#define __gf16_exp CRYPTO_NAMESPACE(__gf16_exp)
#define __gf16_log CRYPTO_NAMESPACE(__gf16_log)
#define __gf16_mul CRYPTO_NAMESPACE(__gf16_mul)
#define __gf256_mul CRYPTO_NAMESPACE(__gf256_mul)
#define __gf16_mulx2 CRYPTO_NAMESPACE(__gf16_mulx2)
''' ] ,
'blas_comm.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define gf256v_set_zero   CRYPTO_NAMESPACE(gf256v_set_zero)
#define gf256v_is_zero   CRYPTO_NAMESPACE(gf256v_is_zero)
''' ] ,
'rainbow_keypair_computation.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define extcpk_to_pk CRYPTO_NAMESPACE(  extcpk_to_pk )
#define calculate_Q_from_F CRYPTO_NAMESPACE(  calculate_Q_from_F )
#define calculate_Q_from_F_cyclic CRYPTO_NAMESPACE(  calculate_Q_from_F_cyclic )
''' ] ,
'rainbow_keypair_computation_simd.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define calculate_Q_from_F_simd CRYPTO_NAMESPACE( calculate_Q_from_F_simd )
#define calculate_F_from_Q_simd CRYPTO_NAMESPACE( calculate_F_from_Q_simd )
#define calculate_Q_from_F_cyclic_simd CRYPTO_NAMESPACE( calculate_Q_from_F_cyclic_simd )
''' ] ,
'parallel_matrix_op.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define UpperTrianglize CRYPTO_NAMESPACE( UpperTrianglize )
#define batch_trimat_madd_gf16 CRYPTO_NAMESPACE( batch_trimat_madd_gf16 )
#define batch_trimat_madd_gf256 CRYPTO_NAMESPACE( batch_trimat_madd_gf256 )
#define batch_trimatTr_madd_gf16 CRYPTO_NAMESPACE( batch_trimatTr_madd_gf16 )
#define batch_trimatTr_madd_gf256 CRYPTO_NAMESPACE( batch_trimatTr_madd_gf256 )
#define batch_2trimat_madd_gf16 CRYPTO_NAMESPACE( batch_2trimat_madd_gf16 )
#define batch_2trimat_madd_gf256 CRYPTO_NAMESPACE( batch_2trimat_madd_gf256 )
#define batch_matTr_madd_gf16 CRYPTO_NAMESPACE( batch_matTr_madd_gf16 )
#define batch_matTr_madd_gf256 CRYPTO_NAMESPACE( batch_matTr_madd_gf256 )
#define batch_bmatTr_madd_gf16 CRYPTO_NAMESPACE( batch_bmatTr_madd_gf16 )
#define batch_bmatTr_madd_gf256 CRYPTO_NAMESPACE( batch_bmatTr_madd_gf256 )
#define batch_mat_madd_gf16 CRYPTO_NAMESPACE( batch_mat_madd_gf16 )
#define batch_mat_madd_gf256 CRYPTO_NAMESPACE( batch_mat_madd_gf256 )
#define batch_quad_trimat_eval_gf16 CRYPTO_NAMESPACE( batch_quad_trimat_eval_gf16 )
#define batch_quad_trimat_eval_gf256 CRYPTO_NAMESPACE( batch_quad_trimat_eval_gf256 )
#define batch_quad_recmat_eval_gf16 CRYPTO_NAMESPACE( batch_quad_recmat_eval_gf16 )
#define batch_quad_recmat_eval_gf256 CRYPTO_NAMESPACE( batch_quad_recmat_eval_gf256 )
''' ] ,
'parallel_matrix_op_sse.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define batch_trimat_madd_multab_gf16_sse CRYPTO_NAMESPACE( batch_trimat_madd_multab_gf16_sse )
#define batch_trimat_madd_multab_gf256_sse CRYPTO_NAMESPACE( batch_trimat_madd_multab_gf256_sse )
#define batch_trimatTr_madd_multab_gf16_sse CRYPTO_NAMESPACE( batch_trimatTr_madd_multab_gf16_sse )
#define batch_trimatTr_madd_multab_gf256_sse CRYPTO_NAMESPACE( batch_trimatTr_madd_multab_gf256_sse )
#define batch_2trimat_madd_multab_gf16_sse CRYPTO_NAMESPACE( batch_2trimat_madd_multab_gf16_sse )
#define batch_2trimat_madd_multab_gf256_sse CRYPTO_NAMESPACE( batch_2trimat_madd_multab_gf256_sse )
#define batch_matTr_madd_multab_gf16_sse CRYPTO_NAMESPACE( batch_matTr_madd_multab_gf16_sse )
#define batch_matTr_madd_multab_gf256_sse CRYPTO_NAMESPACE( batch_matTr_madd_multab_gf256_sse )
#define batch_bmatTr_madd_multab_gf16_sse CRYPTO_NAMESPACE( batch_bmatTr_madd_multab_gf16_sse )
#define batch_bmatTr_madd_multab_gf256_sse CRYPTO_NAMESPACE( batch_bmatTr_madd_multab_gf256_sse )
#define batch_mat_madd_multab_gf16_sse CRYPTO_NAMESPACE( batch_mat_madd_multab_gf16_sse )
#define batch_mat_madd_multab_gf256_sse CRYPTO_NAMESPACE( batch_mat_madd_multab_gf256_sse )
#define batch_quad_trimat_eval_multab_gf16_sse CRYPTO_NAMESPACE( batch_quad_trimat_eval_multab_gf16_sse )
#define batch_quad_trimat_eval_multab_gf256_sse CRYPTO_NAMESPACE( batch_quad_trimat_eval_multab_gf256_sse )
''' ] ,
'parallel_matrix_op_avx2.h': [ '//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE',
'''
#define batch_trimat_madd_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_trimat_madd_multab_gf16_avx2 )
#define batch_trimat_madd_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_trimat_madd_multab_gf256_avx2 )
#define batch_trimatTr_madd_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_trimatTr_madd_multab_gf16_avx2 )
#define batch_trimatTr_madd_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_trimatTr_madd_multab_gf256_avx2 )
#define batch_2trimat_madd_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_2trimat_madd_multab_gf16_avx2 )
#define batch_2trimat_madd_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_2trimat_madd_multab_gf256_avx2 )
#define batch_matTr_madd_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_matTr_madd_multab_gf16_avx2 )
#define batch_matTr_madd_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_matTr_madd_multab_gf256_avx2 )
#define batch_bmatTr_madd_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_bmatTr_madd_multab_gf16_avx2 )
#define batch_bmatTr_madd_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_bmatTr_madd_multab_gf256_avx2 )
#define batch_mat_madd_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_mat_madd_multab_gf16_avx2 )
#define batch_mat_madd_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_mat_madd_multab_gf256_avx2 )
#define batch_quad_trimat_eval_multab_gf16_avx2 CRYPTO_NAMESPACE( batch_quad_trimat_eval_multab_gf16_avx2 )
#define batch_quad_trimat_eval_multab_gf256_avx2 CRYPTO_NAMESPACE( batch_quad_trimat_eval_multab_gf256_avx2 )
''' ] ,
'parallel_matrix_op.c':
   ['//IF_CRYPTO_CORE:EOF' ,
    '//IF_CRYPTO_CORE:EOF' ] ,
'parallel_matrix_op_sse.c':
   ['//IF_CRYPTO_CORE:EOF' ,
    '//IF_CRYPTO_CORE:EOF' ] ,
'parallel_matrix_op_avx2.c':
   ['//IF_CRYPTO_CORE:EOF' ,
    '//IF_CRYPTO_CORE:EOF' ] ,
}



def change_file_content( file_name , symbol , newphrase ):
  try:
    content = list( open( file_name ) )
  except :
    raise
  eof = -1
  for i in range( len(content) ) :
    if content[i].startswith( symbol ):
        content[i] = newphrase + '\n'
    if content[i].startswith( '//IF_CRYPTO_CORE:EOF' ):
        eof = i
  if( 0 <= eof ) : content = content[:eof]
  try:
    fp = open( file_name , "w" )
  except :
    return False
  for l in content: fp.write( l )
  fp.close()
  return True


for dest in dest_dirs :
  shutil.copy2( 'used' , dest + '/used' )
  for imp in impls :
    d1 = dest + '/' + imp
    print( 'copying api.h  -> dir: '  + d1 )
    shutil.copy2( 'api_crypto_core.h' , d1 + '/api.h' )
    shutil.copy2( 'goal-constbranch'  , d1  )
    shutil.copy2( 'goal-constindex'   , d1  )
    for fn in os.listdir(d1) :
      rfn = d1 + '/' + fn
      if fn in white_list :
        print( 'file:' + rfn  + ' ... o' )
	if fn in replace_dic :
          print( 'replace file content: ' + rfn )
          change_file_content( rfn , *replace_dic[fn] )
      else:
        print( 'file:' + rfn  + ' ... x' )
        os.remove( rfn )


