#!/bin/bash

if [ $# != 2 ]; then
  echo "Usage: ./exe  dir1  dir2"
  exit 1
fi

echo 'dir1: '
echo $1
echo 'dir2: '
echo $2

echo "=== check Ia Classic ==="
DIR='Ia_Classic'
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check Ia Cyclic ==="
DIR="Ia_Cyclic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check Ia Cyclic2 ==="
DIR="Ia_CompressedCyclic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check IIIc Classic ==="
DIR="IIIc_Classic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check IIIc Cyclic ==="
DIR="IIIc_Cyclic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check IIIc Cyclic2 ==="
DIR="IIIc_CompressedCyclic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check Vc Classic ==="
DIR="Vc_Classic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check Vc Cyclic ==="
DIR="Vc_Cyclic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

echo "=== check Vc Cyclic2 ==="
DIR="Vc_CompressedCyclic"
cmd="diff ${1}/${DIR}/PQCsignKAT_*.rsp ${2}/${DIR}/PQCsignKAT_*.rsp"
${cmd}

